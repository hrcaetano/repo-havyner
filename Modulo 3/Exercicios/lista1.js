// Exercício 1
function calcularCirculo({ raio, tipoCalculo:tipo }){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio);
}

/*
let circulo = {
    raio: 3,
    tipoCalculo: "A"
}

console.log(calcularCirculo(circulo));
*/

// Exercício 2
let bissexto = ano => (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0); 

// console.log(bissexto(2015));
// console.log(bissexto(2016));

// Exercício 3
function somarPares(arrayDeNumeros){

    let resultado = 0;

    for( let i = 0; i < arrayDeNumeros.length; i++ ){
        if( i % 2 === 0 ) {
            resultado += arrayDeNumeros[i];
        }
    }

    return resultado;
}

// console.log(somarPares([1, 56, 4.34, 6 , -2]));
// 3.34

// Exercício 4
let adicionar = numero1 => numero2 => numero1 + numero2;

// console.log(adicionar(3)(4)); 
// 7
// console.log(adicionar(5642)(8749)); 
// 14391

// Exercício 5
let moedas = (function () {
    //tudo é privado
    function imprmirMoeda(params) {
        function arredondar(numero, precisao = 2) {
            const fator = Math.pow(10, precisao);
            return Math.ceil(numero * fator) / fator;
        } const {
            numero,
            separadorMilhar,
            separadorDecimal,
            colocarMoeda,
            colocarNegativo
        } = params        
        let qtdCasasMilhares = 3;
        let StringBuffer = []
        let parteDecimal = arredondar(Math.abs(numero) % 1);
        let parteInteira = Math.trunc(numero);
        let parteInteiraString = Math.abs(parteInteira).toString();
        let parteInteiraTamanho = parteInteiraString.length; let c = 1;
        while (parteInteiraString.length > 0) {
            if (c % qtdCasasMilhares == 0) {
                StringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`);
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
            } else if (parteInteiraString.length < qtdCasasMilhares) {
                StringBuffer.push(parteInteiraString);
                parteInteiraString = '';
            } c++;
        } StringBuffer.push(parteInteiraString);
        let decimalString = parteDecimal.toString().replace('0.', '').padStart(2, 0);
        const numeroFormatado = `${StringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }    
    //tudo é publico
    return {
        imprimirBRL: (numero) =>
            imprmirMoeda({
                numero,
                separadorMilhar: '.',
                separadorDecimal: ',',
                colocarMoeda: numeroFormatado => `R$ ${numeroFormatado}`,
                colocarNegativo: numeroFormatado => `-${numeroFormatado}`
            })
    }
})()
//os parênteses ao fim do bloco tornam a função auto invocada

// console.log(moedas.imprimirBRL(0));
// console.log(moedas.imprimirBRL(3498.99));
// console.log(moedas.imprimirBRL(-3498.99));
// console.log(moedas.imprimirBRL(2313477.0135));