function cardapioIFood(veggie = true, comLactose = true) {
  let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
  ]
  
  if ( comLactose ) {
      cardapio.push('pastel de queijo')
  }
  
  cardapio = [...cardapio, 'pastel de carne', 'empanada de legumes mabijosa'];

  /*
  cardapio.concat([
      'pastel de carne',
      'empada de legumes marabijosa'
  ])
  */

  if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
      cardapio.splice( cardapio.indexOf('enroladinho de salsicha'), 1 )
      cardapio.splice( cardapio.indexOf('pastel de carne'), 1 )
  }

  //resolução ECMA6 ou posterior
  let resultado = cardapio.map(alimento => alimento.toUpperCase());

  //filtra resultados, só retorna somente alimentos com nome maior do que 12 caracteres
  // let resultado = cardapio
  //                         .filter(alimento => alimento.lenth > 12)
  //                         .map(alimento => alimento.toUpperCase());

  //resolução superada
  // let resultadoFinal = []
  
  // for ( let i = 0; i < cardapio.length; i++ ) {
  //     resultadoFinal[i] = cardapio[i].toUpperCase()
  // }

  return resultado;
} 

console.log(cardapioIFood()) // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]