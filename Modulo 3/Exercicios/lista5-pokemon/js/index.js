const pokeApi = new PokeApi();
const insert = document.getElementById( 'insert' );
const button = document.getElementById( 'com-sorte' );
const currentPokemon = document.getElementById( 'id' );
const cookie = document.cookie;

console.log("cookie " + cookie)

const samePokemon = ( value ) => {
  if ( value === currentPokemon ) {
    return true;
  }

  return false;
}

const validation = value => {
  if ( value < 0 || value > 803 ) {
    return false;
  }

  return true;
};

const refreshValues = ( name, id, height, weight, types, stats, image ) => {
  const pokeId = id;
  const pokeName = name;
  const pokeHeight = height;
  const pokeWeight = weight;
  const pokeTypes = types;
  const pokeStats = stats;
  const pokeImage = image;

  pokeId.innerHTML = '';
  pokeName.innerHTML = '';
  pokeHeight.innerHTML = '';
  pokeWeight.innerHTML = '';
  pokeImage.src = './img/whoisthispokemon.png';
  pokeTypes.innerHTML = '';
  pokeStats.innerHTML = '';
}

function renderPokemon( pokemon ) {
  const name = document.getElementById( 'name' );
  const height = document.getElementById( 'height' );
  const weight = document.getElementById( 'weight' );
  const typesArray = pokemon.types;
  const statsArray = pokemon.stats;
  const types = document.getElementById( 'types' );
  const stats = document.getElementById( 'stats' );
  const pokemonImage = document.getElementById( 'poke-img' );

  refreshValues( name, currentPokemon, height, weight, types, stats, pokemonImage );

  name.innerHTML = pokemon.nome.replace( /^./, pokemon.nome[0].toUpperCase() );
  currentPokemon.innerHTML = pokemon.id;
  height.innerHTML = `${ pokemon.height * 10 }cm`;
  weight.innerHTML = `${ pokemon.weight / 10 }kg`;

  pokemonImage.src = pokemon.pokemonImage;
  pokemonImage.style.margin = '0 auto';
  pokemonImage.style.height = '190px';

  typesArray.forEach( elemento => {
    const li = document.createElement( 'li' );
    li.innerHTML = elemento.type.name;
    li.className = `line-item ${ elemento.type.name }`

    types.appendChild( li );
  } );

  statsArray.forEach( elemento => {
    const div = document.createElement( 'div' );
    div.className = 'mt-2';
    div.innerHTML = elemento.base_stat;
    stats.appendChild( div );
  } );

  insert.value = '';
}

const createErrorMessage = () => {
  const divErro = document.getElementById( 'div-erro' );
  const spanArroba = document.createElement( 'span' );

  spanArroba.innerHTML = 'Este campo deve conter um id válido.';
  spanArroba.style.color = 'red';
  spanArroba.style.display = 'block';
  spanArroba.style.textAlign = 'center';
  divErro.insertAdjacentElement( 'afterbegin', spanArroba )

  setInterval( () => {
    spanArroba.style.display = 'none';
  }, 5000 )
};

async function buscar() {
  const valorDigitado = insert.value;

  if ( !samePokemon( valorDigitado ) ) {
    if ( validation( valorDigitado ) ) {
      const pokemonEspecifico = await pokeApi.buscar( valorDigitado );
      const poke = new Pokemon( pokemonEspecifico );
      renderPokemon( poke );
    } else {
      createErrorMessage();
    }
  }
  return false;
}

const createRandomNumber = () => {
  const min = 1;
  const max = 803;

  return Math.floor( Math.random() * ( max - min ) + min );
};

insert.addEventListener( 'blur', () => {
  buscar();
} );

button.addEventListener( 'click', () => {
  insert.value = createRandomNumber();
  buscar();
} );

const createPokeCookie = id => {

}