let pokeApi = new PokeApi();

function buscar() {
    let pokemonEspecifico = pokeApi.buscar(110);
    let poke = new Pokemon( pokemonEspecifico );
    renderizacaoPokemon( poke );
}

function renderizacaoPokemon( pokemon ) {
    let dadosPokemon = document.getElementById('dadosPokemon');
    let nome = dadosPokemon.querySelector('.nome');
    let imgPokemon = dadosPokemon.querySelector('.thumb');
    nome.innerHTML = pokemon.name;
    imgPokemon.src = pokemon.sprites.front_default
}

buscar();