// Exercício 1

function multiplicar(multiplicador, ...valores) {

    let resposta = [];

    valores.map(valor => resposta.push(multiplicador * valor));

    return resposta;
}

// console.log(multiplicar(5, 3, 4)); // [15, 20]     
// console.log(multiplicar(5, 3, 4, 5)); // [15, 20, 25]

// Exercício 2

const inputs = [...document.getElementsByClassName('field')];
const mensagensDeErro = [...document.getElementsByName('mensagem-erro')];
const email = document.getElementById('email');
const submit = document.getElementsByName('submit')[0];

const verificarDezCaracteres = (mensagem, input) =>  {
 
    if( input.value.length < 10 ) {
 
        mensagem.innerHTML = "Este campo deve conter no mínimo 10 caracteres.";
        mensagem.style.color = "red";
        mensagem.style.display = "block";
        mensagem.style.textAlign = "center";
        setInterval(() => {
            mensagem.style.display = "none";
        }, 3000)
    }
}

for( let i = 0; i < inputs.length; i++ ) {
    inputs[i].addEventListener('blur', verificarDezCaracteres.bind(null, mensagensDeErro[i], inputs[i]), false);
}

email.addEventListener('blur', () => {
    const divEmail = document.getElementById('div-email');

    if( !email.value.includes("@") ) {
        
        const spanArroba = document.createElement("span");
        spanArroba.innerHTML = "Este campo deve conter um email válido.";
        spanArroba.style.color = "red";
        spanArroba.style.display = "block";
        spanArroba.style.textAlign = "center";
        divEmail.insertAdjacentElement('afterbegin', spanArroba)

        setInterval(() => {
            spanArroba.style.display = "none";
            }, 3000
        )
    }
});

submit.addEventListener('click', event => {
    const inputs = document.getElementsByClassName('field');
    let flag = false;

    for ( let i = 0; i < inputs.length; i++ ) {
        if( inputs[i].value.length === 0 ) {
            flag = true;
            break;
        }
    }

    if(flag) {
        alert("Para enviar este formulário certifique-se que todos os campos foram preenchidos.");
        event.preventDefault();
    }
})