//console.log("Cheguei", "\nAqui");

var nomeDaVariavel = "valor";
let nomeDaLet = "ValorLet";
const nomeDaConst = "ValorConst";

const nomeDaConst2 = {
    nome: "Marcos",
    idade: 29
}

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Marcos Henrique"; //como o objeto está congelado, esta linha será interpretada e não terá efeito

function nomeDaFuncao(){
    //console.log();
}

/*
function somar() {
    
}
*/

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);
}

// somar(3);
// somar(3, 2);

/*
console.log(1 + 1);
console.log(1 + "1");
*/

function ondeMoro(cidade){
    //console.log("Eu moro em" + cidade + ". E sou feliz.");
    console.log(`Eu moro em cidade ${cidade}. E sou feliz.`);
}

//ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                + "\n"
                +"Ameixa"
                + "\n"
                +"Goiaba"
                + "\n"
                +"Pêssego"
                + "\n";

    let newTexto = `
                Banana
                Ameixa
                Goiaba
                Pêssego
                `;

    console.log(texto);
    console.log(newTexto);
}

//fruteira();

let eu = {
    nome: "Marcos",
    idade: 29,
    altura: 1.85
};

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} e ${pessoa.altura} de altura.`);
}

//quemSou(eu);

let funcaoSomarValores = function(a, b) {
    return a + b;
};

let add = funcaoSomarValores
let resultado = add(3, 5)
//console.log(resultado);

/* destruction
const { nome, idade } = eu
console.log(nome, idade);
*/
 
const { nome:n, idade:i } = eu
// console.log(n, i);

const array = [1, 3, 4, 8];
const [n1, , n3, n2, n4 = 18] = array;
// console.log(n1, n2, n3, n4);

function testarPessoa({ nome, idade, altura }){
    console.log(nome, idade, altura);
}

//testarPessoa(eu);

/*
let a1 = 42;
let b1 = 15;

let aux = a1;
a1 = b1;
b1 = aux;

[a1, b1] = [b1, a1];
*/

let a1 = 42;
let b1 = 15;
let c1 = 99;
let d1 = 109;

[a1, b1, c1, d1] = [b1, d1, a1, c1];

//com este número de parâmetros seriam necessários muitas let aux, então destruction é mais prático

console.log(a1, b1, c1, d1);