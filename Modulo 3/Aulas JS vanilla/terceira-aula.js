function criarSanduiche( pao, recheio, queijo, salada ){
    console.log(`
    Seu Sanduíche tem o pão ${pao} 
    com recheio de ${recheio} 
    e queijo ${queijo} com
    salada de ${salada}`);
}

const ingredientes = ['3 queijos', 'Frango', 'Cheddar', 'Tomate e Alface'];

// Chamado em 40 lugares
// criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

// receberValoresIndefinidos(1, 2, 3, 4, 5, 6);

// console.log(... "Marcos");
// console.log( {... "Marcos"} );
console.log( [... "Marcos"] );

/*******************************************/

// Eventos
//Padrões Window or Document
let inputTeste = document.getElementById('campoTeste');

inputTeste.addEventListener('blur', () => {
    alert("Obrigado");
})