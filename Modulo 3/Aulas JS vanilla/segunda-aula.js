// Correções do Marcos

// Minha resolução, exercício 4
function adicionar(numero1){
    return function(numero2) {
        return numero1 + numero2;
    }
}

// Resolução do Marcos
let adicionar = numero1 => numero2 => numero1 + numero2;

// Verificar se número é divisível
const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;

// console.log(is_divisivel(divisor, 20));
// console.log(is_divisivel(divisor, 11));
// console.log(is_divisivel(divisor, 12));

// Mesma função, usando 'currying'
const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);

// console.log(is_divisivel(20));
// console.log(is_divisivel(11));
// console.log(is_divisivel(12));

// console.log("Exercício 4, com currying:");
// console.log(adicionar(8)(2.5));
// console.log(adicionar(5642)(8749));

