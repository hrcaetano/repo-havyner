// Exercicio 1

function multiplicar(multiplicador, ...valores) {
    return valores.map( valor => multiplicador * valor );
}

// console.log(multiplicar(5, 3, 4));

// Exercicio 2
// Ponto de partida para a resolução

function validacao(event) {
    let target = event.target;
    let msg = '';

    switch (target.name) {
        case "nome":
            if(target.value == '') {
                msg = "Esse campo não pode ser vazio";
            } else {
                msg = '';
            }
            break;
        case "email":
            break;
        case "telefone":
                break;
        default:
            break;
    }

    let erro = document.getElementById(`msg-erro-${target.name}`);
    erro.innerHTML = msg;
}

let vazio = document.getElementsByClassName('vazio');

for (let i = 0; i < vazio.length; i++) {
    vazio[i].addEventListener('blur', (event) => validacao(event));
}