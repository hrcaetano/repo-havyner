import React, { Component } from 'react';
import ListaSeries from './models/ListaSeries';
import Routes from './components/routes';
import './css/App.css';

export default class JsFlix extends Component {
  constructor( props ){
    super (props);
    this.listaSeries = new ListaSeries()

    this.listaSeries.invalidas() 
    this.listaSeries.filtrarPorAno( 2020 ) 
    this.listaSeries.procurarPorNome( 'Winona Ryder' ) 
    this.listaSeries.mediaDeEpisodios() 
    this.listaSeries.totalSalarios( 2 ) 
    this.listaSeries.queroGenero( 'Caos' ) 
    this.listaSeries.queroTitulo( 'The' ) 
    this.listaSeries.creditos( 0 ) 
  }

  render(){
    return (
      <section className="App-section">
        <h3>JS Flix</h3>
        <Routes/>
      </section>
    );
  }
}