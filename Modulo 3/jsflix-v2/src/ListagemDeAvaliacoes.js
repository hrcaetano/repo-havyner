import React, { Component } from 'react';
import Routes from './components/routes';
import ListaAvaliacoes from './models/ListaAvaliacoes';
import './css/App.css';

class Home extends Component {
  constructor( props ){
    super (props);
    this.listaAvaliacoes = new ListaAvaliacoes()

    console.log(this.listaAvaliacoes.apresentarAvaliacoes());
  }

  render() {
    return (
      <section className="App-section">
        <h3>Listagem de Avaliações</h3>
        <Routes />
        <div>
          oi
        </div>
      </section>
    );
  }
}

export default Home;
