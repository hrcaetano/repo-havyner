import React, { Component } from 'react';
import ListaEpisodios from './models/listaEpisodios';
import ListaAvaliacoes from './models/ListaAvaliacoes';
import EpisodioUi from './components/episodioUi';
import MensagemFlash from './components/MensagemFlash';
import MeuInputNumero from './components/MeuInputNumero';
import Routes from './components/routes';
import './css/App.css';

class Home extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.listaAvaliacoes = new ListaAvaliacoes();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      // listaAvaliacoes: this.listaAvaliacoes,
      exibirMensagem: false,
      deveExibirErro: false
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota( { nota, erro } ){
    this.setState({
      deveExibirErro: erro
    })
    if (erro){
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota ) ) {
      episodio.avaliar( nota )
      this.listaAvaliacoes.armazenarAvaliacoes( episodio )
      cor = 'verde'
      mensagem = 'Registramos sua nota!'
    }else{
      cor = 'vermelho'
      mensagem = 'Informar uma nota válida entre 1 e 5!'
    }
    
    this.exibirMensagem( { cor, mensagem } )
  }

  exibirMensagem = ({ cor, mensagem }) => {
    this.setState({
      cor,
      mensagem,
      exibirMensagem: true
    })
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state

    return (
      <div className="App">
        <Routes/>
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ exibirMensagem } 
                        mensagem={ mensagem }
                        cor={ cor }
                        segundos={ 5 } />
         <header className='App-section'>
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
          </div>
          <MeuInputNumero placeholder="1 a 5"
                          mensagemCampo="Qual sua nota para esse episódio?"
                          visivel={ episodio.assistido || false }
                          obrigatorio={ true }
                          atualizarValor={ this.registrarNota.bind( this ) }
                          deveExibirErro={ deveExibirErro }
          />
        </header>
      </div>
    );
  }
}

export default Home;
