import React, { Component } from 'react';
import Routes from './components/routes';
import './css/App.css';

class Home extends Component {
  render() {
    return (
      <section className="App-section">
        <h3>Página Inicial</h3>
        <Routes />
      </section>
    );
  }
}

export default Home;
