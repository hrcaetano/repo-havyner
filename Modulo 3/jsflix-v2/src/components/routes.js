import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Routes extends Component {
    render() {
        return (
            <div className="routes">        
                <span className="link"><Link to="/">Home</Link></span>
                <span className="link"><Link to="/blackmirror">Black Mirror</Link></span>
                <span className="link"><Link to="/jsflix">JS Flix</Link></span>
                <span className="link"><Link to="/listagem">Listagem de Avaliações</Link></span>
            </div>
        )
    }
}