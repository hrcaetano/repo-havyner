import React, { Component } from 'react';
import './App.css';

// import CompA, { CompB } from './components/exemploComponenteBasico';
import Membros from './components/Membros';

class App extends Component {
  constructor( props ) {
    super( props );
  }

  render() {
    return (
      <div className="App">
        {/* <CompA />
        <CompA />
        <CompB />
        <CompA />
        <CompA />
        <CompA /> */}
        <Membros nome='João' sobrenome='Silva' />
        <Membros nome='Maria' sobrenome='Silva' />
        <Membros nome='Pedro' sobrenome='Silva' />
      </div>
    );
  }
}

export default App;
