import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './css/App.css';
import './css/link.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './components/episodioUi';
import MensagemFlash from './components/MensagemFlash';

class BlackMirror extends Component {
  constructor( props ) {
    super( props );
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false,
      notaValida: false,
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    this.setState( {
      episodio
    } )
  }

  assistido() {
    const { episodio } = this.state
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota( evt ){
    const { episodio } = this.state

    this.avaliarNota( evt.target.value )
    episodio.avaliar( evt.target.value )

    this.setState( {
      episodio,
      deveExibirMensagem: true
    } )
  }

  enviarDeveExibir = ( respostaFilho ) => {
    this.setState({
      deveExibirMensagem: respostaFilho
    })
  }

  avaliarNota( nota ) {
    if( nota >= 1 && nota <= 5 ) {
      this.setState( {
        notaValida: true,
        mensagemTexto: "Registramos sua nota!",
        cor: 'verde'
      } )
    } else {
      this.setState( {
        notaValida: false,
        mensagemTexto: "Informar uma nota válida (entre 1 e 5)",
        cor: 'vermelho'
      } )      
    }
  }

  geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Qual sua nota para esse episódio?</span>
              <input type="number" placeholder="1 a 5" onBlur={ this.registrarNota.bind( this ) } ></input>
            </div>
          )
        }
      </div>
    )
  }

  atualizarMensagem = devoExibir => {
    this.setState({
      deveExibirMensagem: devoExibir
    })
  }

  render() {
    const { episodio, deveExibirMensagem, mensagemTexto, cor } = this.state

    return (
      <div className="App">
         <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                        deveExibirMensagem={ deveExibirMensagem} 
                        mensagem={ mensagemTexto }
                        cor={ cor }
                        enviarDeveExibir= { this.enviarDeveExibir.bind(this) } />
         <header className='App-section'>
          <EpisodioUi episodio={ episodio } />
          <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
          </div>
          { this.geraCampoDeNota() }
          <div>
            <span className="link"><Link to="/">Home</Link></span>
            <span className="link"><Link to="/blackmirror">Black Mirror</Link></span>
            <span className="link"><Link to="/jsflix">JS Flix</Link></span>
          </div>
        </header>
      </div>
    );
  }
}

export default BlackMirror;
