import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './css/App.css';
import './css/link.css';
import ListaSeries from './models/ListaSeries';

export default class JsFlix extends Component {
  constructor( props ){
    super (props);
    this.listaSeries = new ListaSeries()

    this.listaSeries.invalidas() 
    this.listaSeries.filtrarPorAno( 2020 ) 
    this.listaSeries.procurarPorNome( 'Winona Ryder' ) 
    this.listaSeries.mediaDeEpisodios() 
    this.listaSeries.totalSalarios( 2 ) 
    this.listaSeries.queroGenero( 'Caos' ) 
    this.listaSeries.queroTitulo( 'The' ) 
    this.listaSeries.creditos( 0 ) 
  }

  render(){
    return (
      <section className="App-section">
          <div>        
            <h3>JS Flix</h3>
            <span className="link"><Link to="/">Home</Link></span>
            <span className="link"><Link to="/blackmirror">Black Mirror</Link></span>
            <span className="link"><Link to="/jsflix">JS Flix</Link></span>
        </div>
      </section>
    );
  }
}