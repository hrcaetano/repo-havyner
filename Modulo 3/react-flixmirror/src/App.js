import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import BlackMirror from './BlackMirror';
import Home from './Home';
import JsFlix from './JsFlix';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/blackmirror" component={ BlackMirror } />
        <Route path="/jsflix" component={ JsFlix } />
      </Router>
    );
  }
}
