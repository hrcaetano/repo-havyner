import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './css/App.css';
import './css/link.css';

class Home extends Component {
  render() {
    return (
      <section className="App-section">
        <div>        
          <h3>Página inicial</h3>
          <span className="link"><Link to="/">Home</Link></span>
          <span className="link"><Link to="/blackmirror">Black Mirror</Link></span>
          <span className="link"><Link to="/jsflix">JS Flix</Link></span>
        </div>
      </section>
    );
  }
}

export default Home;
