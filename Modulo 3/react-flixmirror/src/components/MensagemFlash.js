import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MensagemFlash extends Component {

    fechar = () => {
        this.props.atualizarMensagem( false )
    }

    enviarResposta() {
        setTimeout( () => {
            this.props.enviarDeveExibir(false);
        }, 5000 )
    }

    render(){
        const { deveExibirMensagem, mensagem, cor } = this.props
        this.enviarResposta()

        return (
            <span onClick={ this.fechar } className={ `flash ${ cor } ${ deveExibirMensagem ? '' : 'invisivel' }` }>{ mensagem }</span>
        )
    }
}

MensagemFlash.propTypes = {
    // mensagem: PropTypes.string.isRequired,
    deveExibirMensagem: PropTypes.bool.isRequired,
    atualizarMensagem: PropTypes.func.isRequired,
    cor: PropTypes.string
}

MensagemFlash.defaultProps = {
    cor: 'verde'
}