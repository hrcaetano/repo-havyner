import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './home';
import AboutUs from './about-us';
import Solutions from './solutions';
import ContactUs from './contact-us';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Route path="/" exact component={ Home } />
        <Route path="/solutions" component={ Solutions } />
        <Route path="/aboutus" component={ AboutUs } />
        <Route path="/contactus" component={ ContactUs } />
      </Router>
    );
  }
}