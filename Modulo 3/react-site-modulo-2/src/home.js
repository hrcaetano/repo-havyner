import React, { Component } from 'react';
import './css/general.css';
import './css/reset.css';
import './css/grid.css';
import './css/box.css';

import Card from './components/card';
import Article from './components/article';
import Banner from './components/banner';
import Header from './components/header';
import Footer from './components/footer';

class Home extends Component {
  constructor( props ) {
    super( props );
    this.state = {

    }
  }

  render() {

    return (
      <div className="App">
        <Header />
        <Banner />
        <section className="container">
            <div className="row">
              <div className="col col-12 col-md-7">
                <Article />
              </div>
              <div className="col col-12 col-md-5">
                <Article />
              </div>
            </div>
            <div className="row">
              <div className="col col-12 col-md-6 col-lg-3" >
                <Card className="box"/>
              </div>
              <div className="col col-12 col-md-6 col-lg-3" >  
                <Card className="box"/>
              </div>
            </div>
            <div className="row">
              <div className="col col-12 col-md-6 col-lg-3">
                <Card className="box"/>
              </div>
              <div className="col col-12 col-md-6 col-lg-3" > 
                <Card className="box"/>
              </div>
            </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Home;