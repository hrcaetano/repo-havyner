import React, { Component } from 'react';
import './css/general.css';
import './css/reset.css';
import './css/grid.css';
import './css/box.css';
import './css/work.css';
import './css/contact.css';

import Iframe from './components/iframe';
import Form from './components/form';
import Header from './components/header';
import Header2 from './components/header2';
import Footer from './components/footer';
import Paragraph from './components/paragraph';

class ContactUs extends Component {
  constructor( props ) {
    super( props );
    this.state = {

    }
  }

  render() {

    return (
      <div className="App">
        <Header />
        <section className="container work">
          <div class="row">
            <div class="col-12 col-md-5">
              <Header2 />
              <Paragraph />
              <Form />              
            </div>
            <div class="col-12 col-md-5 map-container">
              <Iframe />
            </div>                                 
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default ContactUs;