import React, { Component } from 'react';
import './css/general.css';
import './css/reset.css';
import './css/grid.css';
import './css/box.css';
import './css/work.css';

import Card from './components/card';
import Header from './components/header';
import Footer from './components/footer';

class Solutions extends Component {
  constructor( props ) {
    super( props );
    this.state = {

    }
  }

  render() {

    return (
      <div className="App">
        <Header />
        <section className="container work">
          <div class="row">
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />
            </div>
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />  
            </div>
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />  
            </div>
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />  
            </div>
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />  
            </div>
            <div class="col col-12 col-md-6 col-lg-4">
              <Card />  
            </div>                                                
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Solutions;