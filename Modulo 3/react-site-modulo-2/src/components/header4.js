import React, { Component } from 'react';

export default class Header4 extends Component {
    render() {
        const { headerText } = this.props

        return (
            <React.Fragment>
                <h4>
                    { headerText }
                </h4>
            </React.Fragment>
        )
    }
}