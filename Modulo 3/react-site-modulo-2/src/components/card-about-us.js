import React, { Component } from 'react';
import Image from './image'
import Paragraph from './paragraph';
import Header2 from './header2';
import src from '../img/agil.png';

export default class CardAboutUs extends Component {
    render() {
        const alt = 'Ágil';
        const text = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.';
        const headerText = 'Título';

        return (
            <React.Fragment>
                <article>
                    <div>
                        <Image src={ src } alt={ alt }/>
                    </div>
                    <Header2 headerText={ headerText }/> 
                    <Paragraph text={ text }/>
                </article>
            </React.Fragment>
        )
    }
}