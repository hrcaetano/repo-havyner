import React, { Component } from 'react';
import Paragraph from './paragraph';
import Header2 from './header2';

export default class Article extends Component {
    render() {
        const text = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.';
        const headerText = 'Título';

        return (
            <React.Fragment>
                <article>
                    <Header2 headerText={ headerText }/> 
                    <Paragraph text={ text }/>
                </article>
            </React.Fragment>
        )
    }
}