import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/header.css';
import logo from '../img/logo-dbc-topo.png';

export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">
                    <a className="logo" href="index.html" title="Voltar à home">
                        <img src={ logo } alt="DBC Company" />
                    </a>
                    <label className="mobile-menu" htmlFor="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox" />
                    <ul className="clearfix">
                        <li>
                            <Link to="/">Home</Link>              
                        </li>
                        <li>
                            <Link to="/aboutus">Sobre nós</Link>
                        </li>
                        <li>
                            <Link to="/solutions">Serviços</Link>
                        </li>
                        <li>
                            <Link to="/contactus">Contato</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }
}