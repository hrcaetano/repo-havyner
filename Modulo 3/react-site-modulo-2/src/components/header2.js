import React, { Component } from 'react';

export default class Header2 extends Component {
    render() {
        const { headerText } = this.props

        return (
            <React.Fragment>
                <h2>
                    { headerText }
                </h2>
            </React.Fragment>
        )
    }
}