import React, { Component } from 'react';

export default class Image extends Component {
    render() {
        const { src, alt } = this.props

        return (
            <React.Fragment>
                <img src={ src } alt={ alt }/>
            </React.Fragment>
        )
    }
}