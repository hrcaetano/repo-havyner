import React, { Component } from 'react';
import Input from './input';
import '../css/buttons.css';

export default class Paragraph extends Component {
    render() {
        return (
            <form className="clearfix">
                <Input type={ 'text' } placeholderText={ 'Nome' }/>
                <Input type={ 'text' } placeholderText={ 'Email' }/>
                <Input type={ 'text' } placeholderText={ 'Assunto' }/>
                <textarea className="field" placeholder="Mensagem"></textarea>
                <Input className={ "button button-green button-right" } type={ 'submit' } value={ 'Enviar' } />
            </form>
        )
    }
}