import React, { Component } from 'react';
import Paragraph from './paragraph';
import Button from './button';
import Header1 from './header1';
import '../css/banner.css';

export default class Banner extends Component {
    render() {
        const headerText = 'Vem ser DBC';
        const text = 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.';

        return (
            <section class="main-banner">
                <article>
                    <Header1 headerText={ headerText }/>
                    <Paragraph text={ text }/>
                    <Button />
                </article>
            </section>
        )
    }
}