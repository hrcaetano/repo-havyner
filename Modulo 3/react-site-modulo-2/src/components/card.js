import React, { Component } from 'react';
import Image from './image'
import Paragraph from './paragraph';
import Button from './button';
import Header4 from './header4';
import src from '../img/agil.png';

export default class Card extends Component {
    render() {
        // const { src, alt } = this.props
        const alt = 'Ágil';
        const text = 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.';
        const headerText = 'Título';

        return (
            <React.Fragment>
                <article className="box">
                    <div>
                        <Image src={ src } alt={ alt }/>
                    </div>
                    <Header4 headerText={ headerText }/> 
                    <Paragraph text={ text }/>
                    <Button />
                </article>
            </React.Fragment>
        )
    }
}