import React, { Component } from 'react';

export default class Input extends Component {
    render() {
        const { className, placeholderText, type, value } = this.props

        return (
            <input className={`field ${ className }`} type={ type } placeholder={ placeholderText } value={ value } />
        )
    }
}