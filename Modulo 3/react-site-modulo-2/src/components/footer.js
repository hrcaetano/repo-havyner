import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/footer.css';

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="container">
                    <nav>
                        <ul>
                            <li>
                                <Link to="/">Home</Link>              
                            </li>
                            <li>
                                <Link to="/aboutus">Sobre nós</Link>
                            </li>
                            <li>
                                <Link to="/solutions">Serviços</Link>
                            </li>
                            <li>
                                <Link to="/contactus">Contato</Link>
                            </li>
                        </ul>
                    </nav>
                    <p>
                        &copy; Copyright DBC Company - 2019
                    </p>
                </div>
            </footer>
        )
    }
}