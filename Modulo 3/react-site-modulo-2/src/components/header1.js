import React, { Component } from 'react';

export default class Header1 extends Component {
    render() {
        const { headerText } = this.props

        return (
            <React.Fragment>
                <h1>
                    { headerText }
                </h1>
            </React.Fragment>
        )
    }
}