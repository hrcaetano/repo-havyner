import React, { Component } from 'react';
import './css/general.css';
import './css/reset.css';
import './css/grid.css';
import './css/box.css';
import './css/about-us.css';

import CardAboutUs from './components/card-about-us';
import Header from './components/header';
import Footer from './components/footer';

class AboutUs extends Component {
  constructor( props ) {
    super( props );
    this.state = {

    }
  }

  render() {

    return (
      <div className="App">
        <Header />
        <section className="container about-us">
          <div className="row">
            <CardAboutUs className="col col-12"/>
          </div>
          <div className="row">
            <CardAboutUs className="col col-12"/>
          </div>
          <div className="row">
            <CardAboutUs className="col col-12"/>
          </div>
          <div className="row">
            <CardAboutUs className="col col-12"/>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default AboutUs;