import React from "react";
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import ListagemAgencias from './pages/ListagemAgencias/ListagemAgencias';
import Agencia from './pages/Agencia/Agencia';
import ListagemClientes from './pages/ListagemClientes/ListagemClientes';
import Cliente from './pages/Cliente/Cliente';
import ListagemTiposDeConta from './pages/ListagemTiposDeConta/ListagemTiposDeConta';
import TipoDeConta from './pages/TipoDeConta/TipoDeConta';
import ListagemContaCliente from './pages/ListagemContaCliente/ListagemContaCliente';
import ContaCliente from './pages/ContaCliente/ContaCliente';
import { isAuthenticated } from './util/token'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render = { props =>
      isAuthenticated() ? ( <Component {...props} /> ) : (
        <Redirect to={
            { 
                pathname: "/", 
                state: 
                    { 
                      from: props.location 
                    } 
            }} />
      )
    }
  />
);
const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={ Login } />
      <Route exact path="/login" component={ Login } />
      <PrivateRoute path="/home" exact component={ Home } />
      <PrivateRoute path="/listagem-agencias" component={ ListagemAgencias } />
      <PrivateRoute path="/agencia/:id" component={ Agencia } /> 
      <PrivateRoute path="/listagem-clientes" component={ ListagemClientes } /> 
      <PrivateRoute path="/cliente/:id" component={ Cliente } /> 
      <PrivateRoute path="/listagem-tipos-conta" component={ ListagemTiposDeConta } />
      <PrivateRoute path="/conta/:id" component={ TipoDeConta } /> 
      <PrivateRoute path="/listagem-conta-cliente" component={ ListagemContaCliente } /> 
      <PrivateRoute path="/conta-cliente/:id" component={ ContaCliente } /> 
      <Route path="*" component={ () => <h1>Erro 404 - Página não encontrada</h1> } />
    </Switch>
  </Router>
);

export default Routes;