import React, { Component } from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Card from '../../components/card';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import bank from '../../img/bank.jpg'

export default class Agencia extends Component {
  render() {
    const { agencia } = this.props.location.state
    const { cidade, bairro, logradouro, numero, uf } = agencia.endereco

    return (
      <div className="App">
        <Header />
        <section className="container">
          <div className="row">
            <h3>A sua agência, agora em detalhes</h3>
            <Card>
              <img src={ bank } alt="Bank logo"/>
              <div>
                  <h4>Agência: { agencia.nome }</h4>
                  <ul>
                      <li>Código: { agencia.codigo }</li>
                      <li>Cidade: { cidade }</li>
                      <li>Bairro: { bairro }</li>
                      <li>Endereço: { logradouro }, { numero }</li>
                      <li>UF: { uf }</li>
                  </ul>
              </div>
            </Card>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

