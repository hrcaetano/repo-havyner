import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import axios from 'axios';
import { login } from  '../../util/token';
import './login.css';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import '../../css/listagem.css';
import loginImage from '../../img/login-image.svg'

class Login extends Component {
  state = {
    email: "",
    senha: "",
    error: ""
  };

  validarLogin = async e => {
    e.preventDefault();
    const { email, senha } = this.state;
    if ( !email || !senha ) {
      this.setState({ 
        error: "Preencha e-mail e senha para continuar." 
      });
    } else {
      try {
        const response = await axios.post("http://localhost:1337/login", { email, senha });
        login(response.data.token);
        this.props.history.push("/home");
      } catch (err) {
        this.setState({
          error:
            "Login inválido."
        });
      }
    }
  };

  render() {
    return (
      <section className="container">
        <div className="row">
          <div className="col-6 image-box">
            <h1>Digital Bank</h1>
            <img src={ loginImage } alt="Minimalist drawing of a bank"/>
          </div>
          <div className='form-box'>
            <h3>Faça o login</h3>
            <form onSubmit={this.validarLogin}>
              { this.state.error && <span className="spanErro">{ this.state.error }</span> }
              <input
                type="email"
                placeholder="E-mail"
                onChange={e => this.setState({ email: e.target.value })}
              />
              <input
                type="password"
                placeholder="Senha"
                onChange={e => this.setState({ senha: e.target.value })}
              />
              <button type="submit">Entrar</button>
            </form>
          </div>
        </div>
      </section>
    );
  }
}

export default withRouter(Login);