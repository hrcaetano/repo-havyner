import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from '../../util/Axios';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Article from '../../components/article';
import InputFilter from '../../components/input-filter';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import '../../css/listagem.css';
import bank from '../../img/bank.jpg'

export default class ListagemAgencias extends Component {
  constructor( props ) {
    super( props );
    this.axios = new Axios();
    this.state = {
        agencias: [],
        isLoaded: false
    }
  }

  getAgencias = async () => {
    try {
        const response = await this.axios.buscaAgencias()
        this.setState(
          {
            agencias: response.data.agencias,
            isLoaded: true
          }
        )
      }
      catch (error) {
        return console.log("Não foi possível obter as agências.");
      } 
  }

  filtrar(event){
    const valorParaFiltrar = event.target.value.toString().toLowerCase();;

    const agenciasFiltradas = this.state.agencias.filter( agencia => 
                                    agencia.nome.toLowerCase().includes(valorParaFiltrar) ? agencia : '' );

    this.setState({
        agencias: agenciasFiltradas
    })
  }

  componentDidMount() {
    this._asyncRequest = this.getAgencias();
  }

  // componentWillUnmount() {
  //   if (this._asyncRequest) {
  //     this._asyncRequest.cancel();
  //   }
  // }

  render(){
    const { agencias, isLoaded } = this.state;
    const descricao = ` Index benchmark yield market index tax passively bills. Prices improve shares income. Dividends funds bondholders notes NASDAQ investment grade bonds issuer upswing private. Holder prices interest. Securities retirement Nikkei rollover potential market index 401k credit quality risk municipal managed.`;
    if( !isLoaded ) {
      return <div>Erro na autenticação.</div>
    } else {
      return (
        <div className="App">
          <Header />
          <section className="container">
            <div className="row">
            <h1>Agências cadastradas</h1>
              <div className="col-12">
                <InputFilter idLabel="filter-agencias" 
                            labelText="Filtrar agências por nome: "
                              funcao={ this.filtrar.bind( this ) }/>
                <div className="row">
                {
                  agencias.map( agenciaElemento => 
                    <Link style={{ textDecoration: 'none', color: 'inherit' }} to={ 
                                { 
                                  pathname: `/agencia/${ agenciaElemento.id }`,
                                  state: { agencia: agenciaElemento } 
                              }  
                            } 
                            key={agenciaElemento.id}>
                      <Article>
                        <h2>{ agenciaElemento.nome }</h2> 
                        <img src={ bank } alt="Bank logo"/>
                        <p>{ descricao }</p>
                      </Article>
                    </Link>)
                  }
                </div>   
              </div>
            </div>
          </section>
          <Footer />
        </div>

      ) 
    }
  }
}