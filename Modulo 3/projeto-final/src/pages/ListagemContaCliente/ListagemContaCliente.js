import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from '../../util/Axios';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Article from '../../components/article';
import InputFilter from '../../components/input-filter';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import '../../css/listagem.css';
import accountClient from '../../img/account-client.svg'

export default class ListagemContaCliente extends Component {
  constructor( props ) {
    super( props );
    this.axios = new Axios();
    this.state = {
        contasClientes: [],
        isLoaded: false,
    }
  }

  getContasClientes = async () => {
    try {
        const response = await this.axios.buscaContasClientes()
        this.setState(
          {
            contasClientes: response.data.cliente_x_conta,
            contaCliente: {},
            isLoaded: true
          }
        )
      }
      catch (error) {
        return console.log("Não foi possível obter os clientes.");
      } 
  }

  filtrar(event){
    const valorParaFiltrar = parseInt(event.target.value);

    const contasClientesFiltrados = this.state.contasClientes.filter( contasCliente => 
                                    contasCliente.id === valorParaFiltrar ? contasCliente : '' );

    this.setState({
        contasClientes: contasClientesFiltrados 
    })
  }

  componentDidMount() {
    this._asyncRequest = this.getContasClientes();
  }

  // componentWillUnmount() {
  //   if (this._asyncRequest) {
  //     this._asyncRequest.cancel();
  //   }
  // }

  render(){
    const { contasClientes, isLoaded } = this.state 

    if( !isLoaded ) {
      return <div>Erro na autenticação.</div>
    } else {
      return (
        <div className="App">
          <Header />
          <section className="container">
            <div className="row">
              <div className="col-12">
                <h1>Contas e Clientes</h1>
                <div className="col-12">
                  <InputFilter idLabel="filter-conta-lientes" 
                                labelText="Conta cliente por código: "
                                  funcao={ this.filtrar.bind( this ) }/>
                    <div className="row">
                      {
                        contasClientes.map( ccElemento => 
                          <Link style={{ textDecoration: 'none', color: 'inherit' }} to={ 
                                      { 
                                        pathname: `/conta-cliente/${ ccElemento.id }`,
                                        state: { contaCliente: ccElemento } 
                                    }  
                                  } 
                                  key={ccElemento.id}>
                            <Article>
                              <img src={ accountClient } alt="Account client icon"/>
                              <h3>Conta Cliente Código: { ccElemento.codigo }</h3> 
                              <h5>{ ccElemento.cliente.nome }</h5> 
                              <h5>Possui uma conta { ccElemento.tipo.nome }</h5>
                            </Article>
                          </Link>)
                      }
                    </div>
                  </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
      ) 
    }
  }
}