import React, { Component } from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import './home.css';
import homeImage from '../../img/home-image.svg';

export default class Home extends Component {
  render() {
    return (
      <div className="App">
        <Header />
        <section className="container">
          <div className="row">
            <div className="col-12 home-content">
              <h1>Digital Bank</h1>
              <img src={ homeImage } alt="Home"/>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

