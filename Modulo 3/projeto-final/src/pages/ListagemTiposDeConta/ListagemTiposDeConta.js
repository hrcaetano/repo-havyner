import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from '../../util/Axios';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Article from '../../components/article';
import InputFilter from '../../components/input-filter';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import '../../css/listagem.css';
import account from '../../img/account.svg'

export default class ListagemTipoContas extends Component {
  constructor( props ) {
    super( props );
    this.axios = new Axios();
    this.state = {
        contas: [],
        isLoaded: false,
    }
  }

  getTipoContas = async () => {
    try {
        const response = await this.axios.buscaTipoContas()
        this.setState(
          {
            contas: response.data.tipos,
            isLoaded: true
          }
        )
      }
      catch (error) {
        return console.log("Não foi possível obter as contas.");
      } 
  }

  filtrar(event){
    const valorParaFiltrar = event.target.value.toString().toLowerCase();

    const contasFiltradas = this.state.contas.filter( conta => 
                                    conta.nome.toLowerCase().includes(valorParaFiltrar) ? conta : '' );

    this.setState({
        contas: contasFiltradas 
    })
  }

  componentDidMount() {
    this._asyncRequest = this.getTipoContas();
  }

  // componentWillUnmount() {
  //   if (this._asyncRequest) {
  //     this._asyncRequest.cancel();
  //   }
  // }

  render(){
    const { contas, isLoaded } = this.state 

    if( !isLoaded ) {
      return <div>Erro na autenticação.</div>
    } else {
      return (
        <div className="App">
          <Header />
          <section className="container">
            <div className="row">
              <div className="col-12">
                <h1>Tipos de contas cadastrados</h1>
                <div className="col-12">
                  <InputFilter idLabel="filter-contas" 
                              labelText="Filtrar contas por tipo: "
                                funcao={ this.filtrar.bind( this ) }/>
                  <div className="row">
                    {
                      contas.map( contaElemento => 
                        <Link style={{ textDecoration: 'none', color: 'inherit' }} to={ 
                                    { 
                                      pathname: `/conta/${ contaElemento.id }`,
                                      state: { conta: contaElemento } 
                                  }  
                                } 
                                key={contaElemento.id}>
                          <Article>
                            <img src={ account } alt="Account icon"/>
                            <h3>{ contaElemento.nome }</h3> 
                          </Article>
                        </Link>)
                    }
                  </div>
                </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>
      ) 
    }
  }
}