import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from '../../util/Axios';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Article from '../../components/article';
import InputFilter from '../../components/input-filter';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import '../../css/listagem.css';
import client from '../../img/client.svg'

export default class ListagemClientes extends Component {
  constructor( props ) {
    super( props );
    this.axios = new Axios();
    this.state = {
        clientes: [],
        isLoaded: false,
    }
  }

  getClientes = async () => {
    try {
        const response = await this.axios.buscaClientes()
        this.setState(
          {
            clientes: response.data.clientes,
            cliente: {},
            isLoaded: true 
          }
        )
      }
      catch (error) {
        return console.log("Não foi possível obter os clientes.");
      } 
  }

  filtrar(event){
    const valorParaFiltrar = event.target.value.toString().toLowerCase();

    const clientesFiltrados = this.state.clientes.filter( cliente => 
                                    cliente.nome.toLowerCase().includes(valorParaFiltrar) ? cliente : '' );

    this.setState({
        clientes: clientesFiltrados
    })
  }

  componentDidMount() {
    this._asyncRequest = this.getClientes();
  }

  // componentWillUnmount() {
  //   if (this._asyncRequest) {
  //     this._asyncRequest.cancel();
  //   }
  // }

  render(){
    const { clientes, isLoaded } = this.state 

    if( !isLoaded ) {
      return <div>Erro na autenticação.</div>
    } else {
      return (
        <div className="App">
          <Header />
          <section className="container">
            <div className="row">
              <div className="col-12">
                <h1>Clientes cadastrados</h1>
                <div className="col-12">
                  <InputFilter idLabel="filter-clientes" 
                              labelText="Filtrar clientes por nome: "
                                funcao={ this.filtrar.bind( this ) }/>
                  <div className="row">
                      {
                        clientes.map( clienteElemento => 
                          <Link style={{ textDecoration: 'none', color: 'inherit' }} to={ 
                                      { 
                                        pathname: `/cliente/${ clienteElemento.id }`,
                                        state: { cliente: clienteElemento } 
                                    }  
                                  } 
                                  key={clienteElemento.id}>
                            <Article>
                              <img src={ client } alt="Client icon"/>
                              <h3>{ clienteElemento.nome }</h3> 
                            </Article>
                          </Link>)
                      }
                    </div>
                  </div>
              </div>
            </div>
          </section>
          <Footer />
        </div>

      ) 
    }
  }
}