import React, { Component } from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Card from '../../components/card';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import account from '../../img/account.svg'

export default class TipoDeConta extends Component {
  render() {
    const { conta } = this.props.location.state

    return (
      <div className="App">
        <Header />
        <section className="container">
          <div className="row">
            <h3>O seu tipo de conta, agora em detalhes</h3>
            <Card>
              <img src={ account } alt="Account icon"/>
              <h4>Tipo de conta: { conta.nome }</h4>
            </Card>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

