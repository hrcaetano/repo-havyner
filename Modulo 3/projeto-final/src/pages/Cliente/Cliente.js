import React, { Component } from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Card from '../../components/card';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import client from '../../img/client.svg'

export default class Cliente extends Component {
  render() {
    const { nome, cpf, agencia } = this.props.location.state.cliente

    return (
      <div className="App">
        <Header />
        <section className="container">
          <div className="row">
            <h3>O seu cliente, agora em detalhes</h3>
            <Card>
              <img src={ client } alt="Client icon"/>
              <div>
                  <h4>Cliente: { nome }</h4>
                  <ul>
                      <li>CPF: { cpf }</li>
                      <li>Agência: { agencia.nome }</li>
                  </ul>
              </div>
            </Card>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

