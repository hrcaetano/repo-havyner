import React, { Component } from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';
import Card from '../../components/card';
import '../../css/general.css';
import '../../css/reset.css';
import '../../css/grid.css';
import '../../css/box.css';
import accountClient from '../../img/account-client.svg'

export default class ContaCliente extends Component {
  render() {
    const { cliente, tipo } = this.props.location.state.contaCliente

    return (
      <div className="App">
        <Header />
        <section className="container">
          <div className="row">
            <h3>Contas e clientes, agora em detalhes</h3>
            <Card>
              <img src={ accountClient } alt="Account icon"/>
              <h3>Conta Cliente Código: { this.props.location.state.contaCliente.codigo }</h3> 
              <h5>Cliente: { cliente.nome }, CPF: { cliente.cpf }</h5>
              <h5>Tipo de conta: { tipo.nome }</h5>
            </Card>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

