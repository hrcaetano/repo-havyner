import React, { Component } from 'react';
import '../css/general.css';
import '../css/card.css';

export default class Card extends Component {
    render() {
        return (
            <div className="card shadow">
                { this.props.children }
            </div> 
        )
    }
}