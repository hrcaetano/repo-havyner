import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/footer.css';

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="container">
                    <nav>
                        <ul>
                            <li>
                                <Link to="/home">Home</Link>              
                            </li>
                            <li>
                                <Link to="/listagem-agencias">Agências</Link>
                            </li>
                            <li>
                                <Link to="/listagem-clientes">Clientes</Link>
                            </li>
                            <li>
                                <Link to="/listagem-tipos-conta">Contas</Link>
                            </li>
                            <li>
                                <Link to="/listagem-conta-cliente">Contas e Clientes</Link>
                            </li>
                        </ul>
                    </nav>
                    <p>
                        &copy; Copyright Digital Bank - 2019
                    </p>
                </div>
            </footer>
        )
    }
}