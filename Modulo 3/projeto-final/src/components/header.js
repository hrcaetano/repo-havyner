import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/header.css';
import '../css/general.css';
import logo from '../img/bank-logo.png';

export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">
                    <Link className="logo" to="/">
                        <img src={ logo } alt="DBC Company" />
                    </Link>
                    <label className="mobile-menu" htmlFor="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox" />
                    <ul className="clearfix">
                        <li>
                            <Link to="/home">Home</Link>              
                        </li>
                        <li>
                            <Link to="/listagem-agencias">Agências</Link>
                        </li>
                        <li>
                            <Link to="/listagem-clientes">Clientes</Link>
                        </li>
                        <li>
                            <Link to="/listagem-tipos-conta">Contas</Link>
                        </li>
                        <li>
                            <Link to="/listagem-conta-cliente">Contas e Clientes</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }
}