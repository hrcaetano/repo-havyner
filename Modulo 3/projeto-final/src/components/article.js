import React, { Component } from 'react';
import '../css/general.css';
import '../css/article.css';

export default class Article extends Component {
    render() {
        return (
            <article className="box shadow">
                { this.props.children }
            </article>
        )
    }
}