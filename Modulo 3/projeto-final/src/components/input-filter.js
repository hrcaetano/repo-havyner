import React, { Component } from 'react';

export default class InputFilter extends Component {
    render() {
        const { idLabel, labelText, funcao } = this.props

        return (
            <div className="col-8 filter-input">
                <label htmlFor={ idLabel }>{ labelText }</label>
                <input type="text" id={ idLabel } onBlur={ funcao }/>
            </div>
        )
    }
}