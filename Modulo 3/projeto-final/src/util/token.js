export const tokenKey = "Authorization";
export const login = token => {  
    sessionStorage.setItem( tokenKey, token );
};
export const getToken = () => sessionStorage.getItem( tokenKey );
export const isAuthenticated = () => sessionStorage.getItem( tokenKey ) !== null;
export const logout = () => {
  sessionStorage.removeItem( tokenKey );
};
export const authToken = getToken();