import axios from 'axios';
import { getToken } from "./token.js";

export default class Axios {
  constructor() {
    this.api = axios.create({
      baseURL: "http://localhost:1337"
    })
    this.api.interceptors.request.use(async config => {
      const token = getToken();
      if (token) {
        config.headers.Authorization = `${token}`
      }
      return config;
    })
  }

  postLogin = (email, password) => this.api.post("/login", { "email": email, "senha":password });

  buscaAgencias = () => axios.get('http://localhost:1337/agencias/', 
  { headers: 
    { Authorization : getToken() } 
  })

  buscaClientes = () => axios.get('http://localhost:1337/clientes/', 
  { headers: 
    { Authorization : getToken() } 
  })

  buscaTipoContas = () => axios.get('http://localhost:1337/tipoContas/', 
  { headers: 
    { Authorization : getToken() } 
  })

  buscaContasClientes = () => axios.get('http://localhost:1337/conta/clientes', 
  { headers: 
    { Authorization : getToken() } 
  })
}