package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;

@Entity
public class ElfoNoturnoEntity extends ElfoEntity {

	public ElfoNoturnoEntity() {
		super.setTipo(Tipo.ELFO_NOTURNO);
		super.setQuantidadeXpAtaque(3);
		super.setQuantidadeDeDano(15.0);
    }
}
