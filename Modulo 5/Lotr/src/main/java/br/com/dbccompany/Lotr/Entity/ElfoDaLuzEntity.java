package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;

@Entity
public class ElfoDaLuzEntity extends ElfoEntity {

	public ElfoDaLuzEntity() {
		super.setTipo(Tipo.ELFO_LUZ);
	}
	
    private int quantidadeAtaques;
    private final double QUANTIDADE_VIDA_GANHA = 10;
    
	public int getQuantidadeAtaques() {
		return quantidadeAtaques;
	}
	
	public void setQuantidadeAtaques(int quantidadeAtaques) {
		this.quantidadeAtaques = quantidadeAtaques;
	}
	
	public double getQUANTIDADE_VIDA_GANHA() {
		return QUANTIDADE_VIDA_GANHA;
	}
}
