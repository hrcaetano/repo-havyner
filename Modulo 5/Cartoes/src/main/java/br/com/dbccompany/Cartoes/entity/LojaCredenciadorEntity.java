package br.com.dbccompany.Cartoes.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table( name = "LOJA_X_CREDENCIADOR" )
@SequenceGenerator( allocationSize = 1, name = "LOJA_X_CREDENCIADOR_SEQ", sequenceName = "LOJA_X_CREDENCIADOR_SEQ")
public class LojaCredenciadorEntity {

	@Id
	@GeneratedValue( generator = "LOJA_X_CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_LOJA_X_CREDENCIADOR")
	private Integer id;
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name = "ID_LOJA",
		joinColumns = {
			@JoinColumn( name = "ID_LOJA_X_CREDENCIADOR")},
		inverseJoinColumns = {
			@JoinColumn(name = "ID_CREDENCIADOR")})
	private List<LojaEntity> lojas = new ArrayList<>();
	
	@ManyToMany( cascade = CascadeType.ALL)
	@JoinTable( name = "ID_CREDENCIADOR",
		joinColumns = {
			@JoinColumn( name = "ID_LOJA_X_CREDENCIADOR")},
		inverseJoinColumns = {
			@JoinColumn(name = "ID_LOJA")})
	private List<CredenciadorEntity> credenciadores = new ArrayList<>();	
	
	private Double taxa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<LojaEntity> getLojas() {
		return lojas;
	}

	public void setLojas(List<LojaEntity> lojas) {
		this.lojas = lojas;
	}

	public List<CredenciadorEntity> getCredenciadores() {
		return credenciadores;
	}

	public void setCredenciadores(List<CredenciadorEntity> credenciadores) {
		this.credenciadores = credenciadores;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}
}
