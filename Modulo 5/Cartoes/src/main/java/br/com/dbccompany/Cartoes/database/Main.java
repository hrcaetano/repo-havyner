package br.com.dbccompany.Cartoes.database;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.dbccompany.Cartoes.entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.entity.CartaoEntity;
import br.com.dbccompany.Cartoes.entity.ClienteEntity;
import br.com.dbccompany.Cartoes.entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.entity.EmissorEntity;
import br.com.dbccompany.Cartoes.entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.entity.LojaCredenciadorEntity;
import br.com.dbccompany.Cartoes.entity.LojaEntity;
//import br.com.dbccompany.Cartoes.entity.LojaXCredenciadorEntity;
import br.com.dbccompany.Cartoes.util.HibernateUtil;

public class Main {
	
	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
//			Criar loja e cliente
			LojaEntity loja = new LojaEntity();
			loja.setNome("H&M");
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Mark");	
			
//			Vender
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Visa");
			bandeira.setTaxa(7.00);
//			bandeira.setTaxa(10.00);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Banco do Brasil");					
			emissor.setTaxa(6.00);
//			emissor.setTaxa(5.00);

			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip("American Chip Manufacturer");
			cartao.setBandeira(bandeira);
			cartao.setEmissor(emissor);
			cartao.setCliente(cliente);
			cartao.setVencimento(new Date());
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setCartao(cartao);
			lancamento.setLoja(loja);
			lancamento.setEmissor(emissor);
			lancamento.setDescricao("Tênis Adidas");
			lancamento.setValor(250.00);
//			lancamento.setValor(400.00);
			lancamento.setDataCompra(new Date());
			
			LojaEntity loja2 = new LojaEntity();
			loja.setNome("Pull & Bear");
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			lojas.add(loja2);
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("Cielo");
			CredenciadorEntity credenciador2 = new CredenciadorEntity();
			credenciador.setNome("GetNet");
			List<CredenciadorEntity> credenciadores = new ArrayList<>();
			credenciadores.add(credenciador);
			credenciadores.add(credenciador2);

			LojaCredenciadorEntity lojaCredenciador = new LojaCredenciadorEntity();
			lojaCredenciador.setCredenciadores(credenciadores);
			lojaCredenciador.setLojas(lojas);
			lojaCredenciador.setTaxa(13.0);
//			lojaCredenciador.setTaxa(10.0);
			
			session.save(credenciador);
			session.save(loja);
			session.save(cliente);
			session.save(bandeira);
			session.save(emissor);	
			session.save(cartao);
			session.save(lancamento);
			session.save(lojaCredenciador);
			
			transaction.commit();
		} catch (Exception e) {
			if ( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			Connection conn = Connector.connect();
			String sql = "SELECT EMISSOR.TAXA as TAXA_EMISSOR, \n" + 
					"        BANDEIRA.TAXA AS TAXA_BANDEIRA, \n" + 
					"        LANCAMENTO.VALOR as VALOR_LANCAMENTO,\n" + 
					"        loja_x_credenciador.taxa AS TAXA_LOJAXCRED\n" + 
					"FROM CARTAO\n" + 
					"INNER JOIN BANDEIRA\n" + 
					"    ON CARTAO.ID_CARTAO = BANDEIRA.ID_BANDEIRA\n" + 
					"INNER JOIN EMISSOR\n" + 
					"    ON CARTAO.ID_CARTAO = EMISSOR.ID_EMISSOR\n" + 
					"INNER JOIN LANCAMENTO\n" + 
					"    ON CARTAO.ID_CARTAO = LANCAMENTO.FK_ID_CARTAO\n" + 
					"INNER JOIN LOJA\n" + 
					"    ON LANCAMENTO.FK_ID_LOJA = LOJA.ID_LOJA\n" + 
					"INNER JOIN LOJA_X_CREDENCIADOR\n" + 
					"    ON LOJA.ID_LOJA = LOJA_X_CREDENCIADOR.ID_LOJA_X_CREDENCIADOR";
			PreparedStatement preparedStatement = conn.prepareStatement(sql);
			ResultSet rs = preparedStatement.executeQuery();

			Double valorProd = 0.0;
			Double taxaCredenciador = 0.0;
			Double taxaBandeira = 0.0;
			Double taxaEmissor = 0.0;
			
			while ( rs.next() ){
				taxaEmissor = rs.getDouble(1);
				taxaBandeira = rs.getDouble(2);
				valorProd = rs.getDouble(3);
				taxaCredenciador = rs.getDouble(4);
			}
			
			Double fatiaCredenciador = valorProd * taxaCredenciador/100;
			Double fatiaBandeira = valorProd * taxaBandeira/100;
			Double fatiaEmissor = valorProd * taxaEmissor/100;

			System.out.println("\n* Resultados *");
			System.out.println("Taxa do credenciador: " + fatiaCredenciador); //32.5
			System.out.println("Taxa do bandeira: " + fatiaBandeira); //17.5
			System.out.println("Taxa do emissor: " + fatiaEmissor + "\n"); //15.0
			
//			Resultados para o segundo grupo de valores, aqueles que estão comentados
//			System.out.println("Taxa do credenciador: " + fatiaCredenciador); //40.0
//			System.out.println("Taxa do bandeira: " + fatiaBandeira); //40.0
//			System.out.println("Taxa do emissor: " + fatiaEmissor); //20.0
			
			transaction.commit();
		} catch (Exception e) {
			if ( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		} finally {
			System.exit(0);
		}		
	}
}