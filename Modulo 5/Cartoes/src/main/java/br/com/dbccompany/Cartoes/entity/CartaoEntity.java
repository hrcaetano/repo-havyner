package br.com.dbccompany.Cartoes.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CARTAO")
public class CartaoEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "CARTAO_SEQ", sequenceName = "CARTAO_SEQ")
	@GeneratedValue( generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CARTAO", nullable = false)
	private Integer id;
	
	private String chip;
	
	private Date vencimento;
	
	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_CLIENTE" )
	private ClienteEntity cliente;
	
	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_BANDEIRA" )
	private BandeiraEntity bandeira;
	
	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_EMISSOR" )
	private EmissorEntity emissor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChip() {
		return chip;
	}

	public void setChip(String chip) {
		this.chip = chip;
	}

	public Date getVencimento() {
		return vencimento;
	}

	public void setVencimento(Date vencimento) {
		this.vencimento = vencimento;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public BandeiraEntity getBandeira() {
		return bandeira;
	}

	public void setBandeira(BandeiraEntity bandeira) {
		this.bandeira = bandeira;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}	
}
