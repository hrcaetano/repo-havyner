package br.com.dbccompany.Cartoes.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="CREDENCIADOR")
public class CredenciadorEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "CREDENCIADOR_SEQ", sequenceName = "CREDENCIADOR_SEQ")
	@GeneratedValue( generator = "CREDENCIADOR_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_CREDENCIADOR", nullable = false)
	private Integer id;
	
	private String nome;

	@ManyToMany( mappedBy = "credenciadores" )
    private List<LojaCredenciadorEntity> credenciadores = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
