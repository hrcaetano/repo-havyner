package br.com.dbccompany.Cartoes.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LANCAMENTO")
public class LancamentoEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName = "LANCAMENTO_SEQ")
	@GeneratedValue( generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_LANCAMENTO", nullable = false)
	private Integer id;
	
	private String descricao;
	
	private Double valor;
	
	private Date dataCompra;
	
	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_CARTAO" )
	private CartaoEntity cartao;

	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_LOJA" )
	private LojaEntity loja;
	
	@ManyToOne( cascade = CascadeType.ALL )
	@JoinColumn( name = "FK_ID_EMISSOR" )
	private EmissorEntity emissor;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}

	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}
}
