package br.com.dbccompany.Cartoes.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="LOJA")
public class LojaEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ")
	@GeneratedValue( generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE )
	@Column(name = "ID_LOJA", nullable = false)
	private Integer id;
	
	private String nome;

	@ManyToMany( mappedBy = "lojas" )
    private List<LojaCredenciadorEntity> lojas = new ArrayList<>();
	
//    @ManyToMany
//    @JoinTable(
//    		  name = "LojaXCredenciador", 
//    		  joinColumns = 
//    		  		@JoinColumn(name = "loja_id"), 
//    		  inverseJoinColumns = 
//    		  		@JoinColumn(name = "credenciador_id"))
//    Set<CredenciadorEntity> credenciadores;
	
//    @OneToMany( mappedBy = "loja")
//    Set<LojaXCredenciadorEntity> taxas;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
