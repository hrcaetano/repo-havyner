package LoTR;

 

import java.util.*;

public class ElfoDaLuz extends Elfo {
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    private int quantidadeAtaques;
    private final double QUANTIDADE_VIDA_GANHA = 10;
    
    {
        quantidadeAtaques = 0;
    }
    
    public ElfoDaLuz( String nome ) {
        super(nome);
        super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
    }
    
    public void perderItem( Item item ) {
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        
        if(possoPerder) {
            super.perderItem(item);
        }
    }
    
    public void atacarComEspada( Dwarf anao ) {
        if( this.getStatus() != Status.MORTO ) {
            this.aumentarXp();
            anao.sofrerDano();
            quantidadeAtaques++; 
            
            if( devePerderVida() ) {
                this.quantidadeDano = 21.0;
                this.sofrerDano();
                this.quantidadeDano = 0.0;
            } else {
                this.ganharVida();
            }
        }
    }    
    
    public void ganharVida() {
        vida += QUANTIDADE_VIDA_GANHA;
    }
    
    public boolean devePerderVida() {
        return quantidadeAtaques % 2 == 1;
    }
}
