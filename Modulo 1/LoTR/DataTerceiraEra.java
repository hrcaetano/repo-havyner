package LoTR;

public class DataTerceiraEra {

    private int dia;
    private int mes;
    private int ano;
    
    public DataTerceiraEra( int dia, int mes, int ano ) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
    }
    
    public int getDia() {
        return this.dia ;
    }
    
    public int getMes () {
        return this.mes ;
    }
    
    public int getAno () {
        return this.ano ;
    }
    
    public boolean isBissexto() {
        if( this.getAno() % 4 == 0 ) {
            if( this.getAno() % 100 == 0 ) {
                if ( this.getAno() % 400 == 0 ) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
