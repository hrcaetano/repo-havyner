package LoTR;

public class Elfo extends Personagem implements Comparable<Elfo> {

     private int indiceFlecha;
     private static int elfosInstanciados;
     
     {
        indiceFlecha = 0;
     }
     
     public Elfo( String nome ) {
         super(nome);
         this.vida = 100.0;
         this.inventario.adicionar( new Item(2, "Flecha para arco longo") );
         this.inventario.adicionar( new Item(1, "Arco longo") );
         Elfo.elfosInstanciados++;
     }     
     
     protected void finalize() throws Throwable {
         Elfo.elfosInstanciados--;
     }
     
     public static int getElfosInstanciados() {
        return Elfo.elfosInstanciados;
     } 
     
     public Item getFlecha() {
         return this.inventario.obter(indiceFlecha);
     }
     
     public int getQuantidadeFlecha() {
         return this.getFlecha().getQuantidade();
     }
     
     public boolean temFlecha() {
         return this.getQuantidadeFlecha() > 0;
     }
     
     public void atirarFlecha( Dwarf anao ) {
         if( temFlecha() ) {
             this.getFlecha().setQuantidade( this.getQuantidadeFlecha() - 1 );
             this.aumentarXp();
             anao.sofrerDano();
             this.sofrerDano();
         }
     }
     
     public String imprimirResumo() {
         return "Hey";
     }
     
     public int compareTo(Elfo elfo) {  
          return this.inventario.busca("Flecha para arco longo").getQuantidade() 
            > elfo.inventario.busca("Flecha para arco longo").getQuantidade() ? -1 : 1;  
     }   
}
