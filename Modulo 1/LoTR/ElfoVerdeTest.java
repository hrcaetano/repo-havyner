package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest {

    @Test
    public void aumentarXpEmDobro() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        Dwarf anao = new Dwarf("Gimli");
        elfoVerde.atirarFlecha(anao);
        
        assertEquals(2, elfoVerde.getExperiencia());
    }
  
    @Test
    public void identificaItemComDescricaoValida() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        Item espada = new Item(1, "Espada de aço valiriano");
        elfoVerde.ganharItem(espada);
        Inventario inventario = elfoVerde.getInventario();
        
        assertEquals(new Item(2, "Flecha para arco longo"), inventario.obter(0));
        assertEquals(new Item(1, "Arco longo"), inventario.obter(1));    
        assertEquals(espada, inventario.obter(2)); 
    }
    
    @Test
    public void identificaItemComDescricaoInvalida() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        Item espadaDeTreino = new Item(1, "Espada de treino");
        elfoVerde.ganharItem(espadaDeTreino);
        Inventario inventario = elfoVerde.getInventario();
        
        assertEquals(new Item(2, "Flecha para arco longo"), inventario.obter(0));
        assertEquals(new Item(1, "Arco longo"), inventario.obter(1));    
        assertNull(inventario.busca("Espada de treino")); 
    }
    
    @Test
    public void perderItemComDescricaoValida() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        Item espada = new Item(1, "Espada de aço valiriano");
        elfoVerde.perderItem(espada);
        Inventario inventario = elfoVerde.getInventario();
        
        assertEquals(new Item(2, "Flecha para arco longo"), inventario.obter(0));
        assertEquals(new Item(1, "Arco longo"), inventario.obter(1));    
        assertNull(inventario.busca("Espada de aço valiriano")); 
    }
    
    @Test
    public void perderItemComDescricaoInvalida() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        Item espadaDeTreino = new Item(1, "Espada de treino");
        elfoVerde.perderItem(espadaDeTreino);
        Inventario inventario = elfoVerde.getInventario();
        
        assertEquals(new Item(2, "Flecha para arco longo"), inventario.obter(0));
        assertEquals(new Item(1, "Arco longo"), inventario.obter(1));
    }    
}
