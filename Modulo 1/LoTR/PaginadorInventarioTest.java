package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest {

    @Test
    public void pularZeroPosicao() {
       Inventario inventario = new Inventario(1);
       PaginadorInventario paginador = new PaginadorInventario(inventario);
       paginador.pular(0);
       
       assertEquals(0, paginador.getPontoDePartida());
    }
    
    @Test
    public void pularDuasPosicoes() {
       Item machadinha = new Item(8, "Machadinha de arremesso");
       Item arco = new Item(1, "Arco longo");
       Item espada = new Item(5, "Espada longa");
       Inventario inventario = new Inventario(1);
       inventario.adicionar(machadinha);
       inventario.adicionar(arco);
       inventario.adicionar(espada);
       PaginadorInventario paginador = new PaginadorInventario(inventario);
       paginador.pular(2);
       
       assertEquals(2, paginador.getPontoDePartida());
    }
    
    @Test
    public void paginarInventarioPulaZeroLimitaDois() {
       Inventario inventario = new Inventario(1);
       Item espada = new Item(8, "Espada longa");
       Item arco = new Item(1, "Arco longo");
       inventario.adicionar(espada);
       inventario.adicionar(arco);
       PaginadorInventario paginador = new PaginadorInventario(inventario);
       paginador.pular(0);
              
       assertEquals("Espada longa", paginador.limitar(2).get(0).getDescricao());
       assertEquals("Arco longo", paginador.limitar(2).get(1).getDescricao());
    }
    
    @Test
    public void paginarInventarioPulaDoisLimitaDois() {
       Inventario inventario = new Inventario(1);
       Item machadinha = new Item(8, "Machadinha de arremesso");
       Item escudo = new Item(4, "Escudo pesado de metal");
       Item espada = new Item(5, "Espada longa");
       Item adaga = new Item(6, "Adaga");
       Item pocao = new Item(2, "Pocao de vida");
       inventario.adicionar(machadinha);
       inventario.adicionar(escudo);
       inventario.adicionar(espada);
       inventario.adicionar(adaga);
       inventario.adicionar(pocao);
       PaginadorInventario paginador = new PaginadorInventario(inventario);
       paginador.pular(2);
              
       assertEquals("Espada longa", paginador.limitar(2).get(0).getDescricao());
       assertEquals("Adaga", paginador.limitar(2).get(1).getDescricao());
    }
    
    @Test
    public void inventarioLimitaDoisRetornaArrayListComTamanhoDois() {
       Inventario inventario = new Inventario(1);
       Item machadinha = new Item(8, "Machadinha de arremesso");
       Item escudo = new Item(4, "Escudo pesado de metal");
       Item espada = new Item(5, "Espada longa");
       Item adaga = new Item(6, "Adaga");
       Item pocao = new Item(2, "Pocao de vida");
       inventario.adicionar(machadinha);
       inventario.adicionar(escudo);
       inventario.adicionar(espada);
       inventario.adicionar(adaga);
       inventario.adicionar(pocao);
       PaginadorInventario paginador = new PaginadorInventario(inventario);
       paginador.pular(2);
              
       assertEquals(2, paginador.limitar(2).size());
    }    
}
