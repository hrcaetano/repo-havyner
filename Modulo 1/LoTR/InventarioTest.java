package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest {
        
    @Test
    public void adicionarUmItem() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
        
        assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void adicionarDoisItens() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals(espada, inventario.getItens().get(0)); 
        assertEquals(escudo, inventario.getItens().get(1)); 
    }
    
    @Test
    public void obterItem() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada");
        inventario.adicionar(espada);
    
        assertEquals(espada, inventario.obter(0));
    }
    
    @Test
    public void removerItem() {
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(1, "Escudo");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        
        assertEquals(2, inventario.getItens().size());
        inventario.remover(espada);
        assertEquals(1, inventario.getItens().size());
    }    
    
    @Test
    public void retornaDescricoesDosItensSeparadosPorVirgula() {
        Inventario inventario = new Inventario(1);
        Item flecha = new Item(1, "Flecha para arco longo");
        Item arco = new Item(1, "Arco longo");
        inventario.adicionar(flecha);
        inventario.adicionar(arco);
        
        assertEquals("Flecha para arco longo, Arco longo", 
        inventario.getDescricoesItens());
    }
    
    @Test
    public void retornaDescricoesNenhumItem() {
        Inventario inventario = new Inventario(1);
        
        assertEquals("", inventario.getDescricoesItens());
    }
    
    @Test
    public void confereSeItemRetornadoPossuiMaiorQuantidade() {
        Inventario inventario = new Inventario(1);
        Item flecha = new Item(2, "Flecha para arco longo");
        Item arco = new Item(1, "Arco longo");
        inventario.adicionar(flecha);
        inventario.adicionar(arco);
               
        assertEquals(2, inventario.getItemComMaiorQuantidade().getQuantidade());
    }
    
    @Test
    public void confereItemMaiorQuantidadeQuantidadesIguais() {
        Inventario inventario = new Inventario(1);
        Item flecha = new Item(1, "Flecha para arco longo");
        Item arco = new Item(1, "Arco longo");
        inventario.adicionar(flecha);
        inventario.adicionar(arco);
               
        assertEquals(flecha, inventario.getItemComMaiorQuantidade());
    }
    
    @Test
    public void confereSeItemRetornadoTemMesmaDescricao() {
        Inventario inventario = new Inventario(1);
        Item flecha = new Item(1, "Flecha para arco longo");
        inventario.adicionar(flecha);   
        
        assertEquals(flecha, inventario.busca("Flecha para arco longo"));
    }
    
    @Test
    public void inverterInventarioVazio() {
        Inventario inventario = new Inventario(1);
        
        assertTrue(inventario.inverter().isEmpty());
    } 
    
    @Test
    public void retornaArrayListDeItensInvertidoUmItem() {
        Inventario inventario = new Inventario(1);
        Item flecha = new Item(1, "Flecha para arco longo");
        inventario.adicionar(flecha);
        
        assertEquals(flecha, inventario.inverter().get(0));
        assertEquals(1, inventario.inverter().size());
    }    
    
    @Test
    public void retornaArrayListDeItensInvertidoDoisItens() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machadinha = new Item(1, "Machadinha de arremesso");
        inventario.adicionar(espada);
        inventario.adicionar(machadinha);
        
        assertEquals(machadinha, inventario.inverter().get(0));
        assertEquals(espada, inventario.inverter().get(1));
    }
    
    @Test
    public void ordenarArrayListQuantidadeAscendenteDeItens() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(5, "Espada longa");
        Item machadinha = new Item(8, "Machadinha de arremesso");
        Item escudo = new Item(4, "Escudo pesado de metal");
        Item adaga = new Item(6, "Adaga");
        Item pocao = new Item(2, "Pocao de vida");
        inventario.adicionar(espada);
        inventario.adicionar(machadinha);
        inventario.adicionar(escudo);
        inventario.adicionar(adaga);
        inventario.adicionar(pocao);
        inventario.ordenarItens();
        
        assertEquals(2, inventario.getItens().get(0).getQuantidade());
        assertEquals(4, inventario.getItens().get(1).getQuantidade());
        assertEquals(5, inventario.getItens().get(2).getQuantidade());
        assertEquals(6, inventario.getItens().get(3).getQuantidade());
        assertEquals(8, inventario.getItens().get(4).getQuantidade());
    }
    
    @Test
    public void retornaArrayOrdenadoComEnumDesc() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machadinha = new Item(4, "Machadinha de arremesso");
        Item escudo = new Item(3, "Escudo pesado de metal");
        Item adaga = new Item(6, "Adaga");
        inventario.adicionar(adaga);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(machadinha);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        
        assertEquals(adaga, inventario.getItens().get(0));
        assertEquals(machadinha, inventario.getItens().get(1));
        assertEquals(escudo, inventario.getItens().get(2));
        assertEquals(espada, inventario.getItens().get(3));
    }
    
    @Test
    public void retornaArrayOrdenadoComEnumAsc() {
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machadinha = new Item(4, "Machadinha de arremesso");
        Item escudo = new Item(3, "Escudo pesado de metal");
        Item adaga = new Item(6, "Adaga");
        inventario.adicionar(adaga);
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(machadinha);
        inventario.ordenarItens(TipoOrdenacao.ASC);
        
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
        assertEquals(machadinha, inventario.getItens().get(2));
        assertEquals(adaga, inventario.getItens().get(3));
    }
    
    @Test
    public void unirDoisInventarios() {
        Inventario inventarioOriginal = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machado = new Item(1, "Machado de batalha");
        inventarioOriginal.adicionar(espada);
        inventarioOriginal.adicionar(machado);   
        
        Inventario inventarioSegundo = new Inventario(1);
        Item adaga = new Item(1, "Adaga");
        Item martelo = new Item(1, "Martelo de combate");
        inventarioSegundo.adicionar(adaga);
        inventarioSegundo.adicionar(martelo);  

        assertEquals(machado, inventarioOriginal.unir(inventarioSegundo).getItens().get(1));
        assertEquals(martelo, inventarioOriginal.unir(inventarioSegundo).getItens().get(3));
    }
    
    @Test
    public void diferenciarDoisInventarios() {
        Inventario inventarioOriginal = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machado = new Item(1, "Machado de batalha");
        inventarioOriginal.adicionar(espada);
        inventarioOriginal.adicionar(machado);   
        
        Inventario inventarioSegundo = new Inventario(1);
        Item adaga = new Item(1, "Adaga");
        Item martelo = new Item(1, "Martelo de combate");
        inventarioSegundo.adicionar(adaga);
        inventarioSegundo.adicionar(martelo);  

        assertEquals(machado, inventarioOriginal.unir(inventarioSegundo).getItens().get(1));
        assertEquals(martelo, inventarioOriginal.unir(inventarioSegundo).getItens().get(3));
    }

    @Test
    public void cruzarDoisInventarios() {
        Inventario inventarioOriginal = new Inventario(1);
        Item espada = new Item(1, "Espada longa");
        Item machado = new Item(1, "Machado de batalha");
        inventarioOriginal.adicionar(espada);
        inventarioOriginal.adicionar(machado);   
        
        Inventario inventarioSegundo = new Inventario(1);
        Item adaga = new Item(1, "Adaga");
        inventarioSegundo.adicionar(adaga);
        inventarioSegundo.adicionar(espada);  

        //System.out.println(inventarioOriginal.cruzar(inventarioSegundo).getItens().get(1).getDescricao());
        //System.out.println(inventarioOriginal.cruzar(inventarioSegundo).getItens().size());
        
        assertEquals(espada, inventarioOriginal.cruzar(inventarioSegundo).getItens().get(0));
    }
}
