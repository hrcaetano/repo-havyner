package LoTR;

import java.util.*;

public class Inventario {
    
    private ArrayList<Item> itens;
    
    public Inventario( int quantidade ) {  
        this.itens = new ArrayList<Item>(quantidade);
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public void adicionar( Item item ) {
        this.itens.add(item);
    }
    
    public Item obter( int posicao ) {
        
        if( posicao >= this.itens.size() ) {
            return null;
        }
        
        return this.itens.get(posicao);
    }
    
    public void remover( Item item ) {
        this.itens.remove(item);
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        
        for( int i = 0; i < this.itens.size(); i++ ) {
            Item item = itens.get(i);
            
            if ( item != null ) {
                descricoes.append(item.getDescricao());
                descricoes.append(", ");
            }
        }
        
        return (descricoes.length() > 0 ? descricoes.substring(0, 
            (descricoes.length() - 2)) : descricoes.toString());
    }
    
    public Item getItemComMaiorQuantidade() {
        int indice = 0, maiorQuantidade = 0;
        
        for( int i = 0; i < this.itens.size(); i++ ) {
            Item item = itens.get(i);
            
            if( item != null) {
                if( item.getQuantidade() > maiorQuantidade ) {
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }            
            }
        }
        
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }
    
    public Item busca( String descricao ) {
        for( Item itemAtual : this.itens ) {
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            
            if( encontrei ) {
                return itemAtual;
            }
        }
        
        return null;
    }
    
    public ArrayList<Item> inverter() {
        ArrayList<Item> itensInvertidos = new ArrayList<>(this.itens.size());
                
        for( int i = this.itens.size() - 1; i >= 0; i-- ) {
            itensInvertidos.add(this.itens.get(i));
        }
            
        return itensInvertidos;
    }
    
    public void ordenarItens() {
        this.ordenarItens( TipoOrdenacao.ASC);
    }
        
    public void ordenarItens( TipoOrdenacao ascOuDesc ) {
        for ( int i = 0; i < this.getItens().size(); i++ ) {
            for ( int j = 0; j < this.getItens().size() - 1; j++ ) {
                Item itemAtual = this.itens.get(j);
                Item itemProximo = this.itens.get(j + 1);
                boolean deveTrocar = ascOuDesc == TipoOrdenacao.ASC ? itemAtual.getQuantidade() > itemProximo.getQuantidade()
                    : itemAtual.getQuantidade() < itemProximo.getQuantidade();
                
                if( deveTrocar ) {
                    Item itemTrocado = itemAtual;
                    this.itens.set(j, itemProximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
    
    public Inventario unir( Inventario inventario ) {
        Inventario inventarioUnificado = new Inventario(1);
        
        for( Item item : this.itens ) {
            inventarioUnificado.getItens().add(item);
        }
        
        for( Item item : inventario.getItens() ) {
            inventarioUnificado.getItens().add(item);
        }
        
        return inventarioUnificado;
    }
    
    public Inventario diferenciar( Inventario inventario ) {
        Inventario inventarioDiferenciado = inventario;
        Item itemInventarioNovo = new Item(1, "placeholder");
        int i = 0;
        
        for( Item item : this.getItens() ) {
            itemInventarioNovo = inventarioDiferenciado.getItens().get(i);
            
            if( item.getDescricao().equals(itemInventarioNovo.getDescricao()) ) {
                inventarioDiferenciado.remover(itemInventarioNovo);
            }
            i++;
        }
        
        return inventarioDiferenciado;
    }
    
    public Inventario cruzar( Inventario inventario ) {
        Inventario inventarioCruzado = new Inventario(1);
        Item itemInventarioOriginal = new Item(1, "placeholder");
        Item itemInventarioNovo = new Item(1, "placeholder");
        
        for( int i = 0; i < this.getItens().size(); i++ ) {
            itemInventarioOriginal = this.getItens().get(i);
            
            for( int j = 0; j < inventario.getItens().size(); j++ ) {
                itemInventarioNovo = inventario.getItens().get(j);
                
                if( itemInventarioOriginal.getDescricao().equals(itemInventarioNovo.getDescricao()) ) {
                    inventarioCruzado.adicionar(itemInventarioNovo);
                }
            }
        }

        return inventarioCruzado;
    }
}
