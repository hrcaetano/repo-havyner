package LoTR;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaAtaqueIntercaladoTest {

    @Test
    public void exercitoEmbaralhadoAtacaIntercalado() {
        EstrategiaAtaqueIntercalado estrategia = new EstrategiaAtaqueIntercalado();
        Elfo night1 = new ElfoNoturno("Noturno x-men 1");
        Elfo night2 = new ElfoNoturno("Noturno x-men 2");
        Elfo green1 = new ElfoVerde("Verde 1");
        Elfo night3 = new ElfoNoturno("Noturno x-men 3");
        Elfo green2 = new ElfoVerde("Verde 2");
        Elfo night4 = new ElfoNoturno("Noturno x-men 4");
        Elfo green3 = new ElfoVerde("Verde 3");
        Elfo green4 = new ElfoVerde("Verde 4");
        ArrayList<Elfo> elfosEnviados = new ArrayList<>(
            Arrays.asList(night1, night2, green1, night3, green2, night4, green3, green4)
        );
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
            Arrays.asList(green1, night1, green2, night2, green3, night3, green4, night4)
        );
        ArrayList<Elfo> elfosResultado = estrategia.getOrdemAtaque(elfosEnviados);
        
        assertEquals(elfosEsperados, elfosResultado);
    }    
}
