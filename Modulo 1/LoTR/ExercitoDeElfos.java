package LoTR;

 

import java.util.*;

public class ExercitoDeElfos {

    private ArrayList<Elfo> exercito = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfoNoturno.class
        )
    );
    
    public ArrayList<Elfo> getExercito() {
        return this.exercito;
    }
    
    public void alistar( Elfo soldado ) {
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(soldado.getClass());
        
        if( podeAlistar ) {
            this.exercito.add(soldado);
            ArrayList<Elfo> elfoDoStatus = porStatus.get( soldado.getStatus() );
            
            if( elfoDoStatus == null ) {
                elfoDoStatus = new ArrayList<>();
                porStatus.put(soldado.getStatus(), elfoDoStatus);
            }
            
            elfoDoStatus.add(soldado);
        }
    }
    
    public ArrayList<Elfo> buscarPorStatus( Status status ) {
        return this.porStatus.get(status);
    }
}
