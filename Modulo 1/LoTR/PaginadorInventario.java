package LoTR;

 

import java.util.*;

public class PaginadorInventario {
 
    private Inventario inventario;
    private int pontoDePartida;
    
    public PaginadorInventario ( Inventario inventario ) {
        this.inventario = inventario;
    } 
    
    public int getPontoDePartida() {
        return this.pontoDePartida;
    }
    
    public void pular( int pontoDePartida ) {
        this.pontoDePartida = pontoDePartida > 0 ? pontoDePartida : 0;
    }
    
    public ArrayList<Item> limitar( int quantidade ) {
        ArrayList<Item> itensPaginados = new ArrayList<>();
        int fim = this.pontoDePartida + quantidade;
        
        for( int i = this.pontoDePartida; i < fim && i < this.inventario.getItens().size(); i++ ) {
            itensPaginados.add(this.inventario.obter(i));
        }
        
        return itensPaginados;
    }
}
