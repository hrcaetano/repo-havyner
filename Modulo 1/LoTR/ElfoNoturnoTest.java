package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {

    @Test
    public void elfoNoturnoTomaDanoAoAtirar() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        Dwarf anao = new Dwarf("Gimli");
        elfoNoturno.atirarFlecha(anao);
        
        assertEquals(85.0, elfoNoturno.getVida(), 1e-9);
    }
    
    @Test
    public void aumentaTresPontosXpAoAtirar() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        Dwarf anao = new Dwarf("Gimli");
        elfoNoturno.atirarFlecha(anao);
        
        assertEquals(3, elfoNoturno.getExperiencia());
    }
    
    @Test
    public void elfoNoturnoAtiraSeteFlechasEMorre() {
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        elfoNoturno.getFlecha().setQuantidade(1000);
        Dwarf anao = new Dwarf("Gimli");
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        elfoNoturno.atirarFlecha(anao);
        
        assertEquals(0.0, elfoNoturno.getVida(), 1e-9);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
    }
}
