package LoTR;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class HobbitContadorTest {

    @Test
    public void hobbitContadorValoresDoExercicio() {
        ArrayList<ArrayList<Integer>> arrayDePares = new ArrayList<>();
        arrayDePares.add(new ArrayList<>(Arrays.asList(15, 18)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(4, 5)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(12, 60)));
        HobbitContador contador = new HobbitContador();
        
        assertEquals(840, contador.calcularDiferenca(arrayDePares));
    }
    
    @Test
    public void hobbitContadorsValores() {
        ArrayList<ArrayList<Integer>> arrayDePares = new ArrayList<>();
        arrayDePares.add(new ArrayList<>(Arrays.asList(17, 23)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(11, 3)));
        arrayDePares.add(new ArrayList<>(Arrays.asList(15, 45)));
        HobbitContador contador = new HobbitContador();
        
        assertEquals(630, contador.calcularDiferenca(arrayDePares));
    }
}
