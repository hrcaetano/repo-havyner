package LoTR;

import java.util.*;

public class HobbitContador {

    private ArrayList<ArrayList<Integer>> arrayDePares;
                                                                          
    public int calcularProduto( int numero1, int numero2 ) {
        return numero1 * numero2;
    }
        
    public Integer calcularMMC( int numero1, int numero2 ) {
        
        int resultado = 1;
        int mmc = 2;
        
        while( mmc <= numero1 || mmc <= numero2 ) {
            if(numero1 % mmc == 0 || numero2 % mmc == 0) {
                resultado *= mmc;
                
                if( numero1 % mmc == 0 ) {
                    numero1 /= mmc;
                }
                if( numero2 % mmc == 0 ) {
                    numero2 /= mmc;
                }
            } else {
                mmc++;
            }
        }                        

        return resultado;
    }
    
    public int calcularDiferenca( ArrayList<ArrayList<Integer>> arrayDePares ) {
        ArrayList<Integer> produtos = new ArrayList<>();
        ArrayList<Integer> minimosMultiplosComuns = new ArrayList<>();
        int somaDosProdutos = 0;
        int somaDosMMCs = 0;
        
        for( ArrayList<Integer> lista : arrayDePares ) {
            produtos.add(calcularProduto( lista.get(0), lista.get(1) ));
            minimosMultiplosComuns.add(calcularMMC( lista.get(0), lista.get(1) ));
        }
        
        for( Integer produto : produtos ) {
            somaDosProdutos += produto;
        }
        
        for( Integer mmc : minimosMultiplosComuns ) {
            somaDosMMCs += mmc;
        }
        
        System.out.println(produtos.get(0));
        System.out.println(produtos.get(1));
        System.out.println(produtos.get(2));
        System.out.println(minimosMultiplosComuns.get(0));
        System.out.println(minimosMultiplosComuns.get(1));
        System.out.println(minimosMultiplosComuns.get(2));
        System.out.println(somaDosProdutos);
        System.out.println(somaDosMMCs);      
        System.out.println(somaDosProdutos - somaDosMMCs); 
        
        return Math.abs(somaDosProdutos - somaDosMMCs);
    }
}
