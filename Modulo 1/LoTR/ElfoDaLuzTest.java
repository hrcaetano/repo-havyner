package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest {

    @Test
    public void nasceComEspadaDeGalvorn() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Auraniel");
        Inventario inventario = elfoDaLuz.getInventario();
        Item espadaDeGalvorn = new Item(1, "Espada de Galvorn");
        
        assertEquals(espadaDeGalvorn, inventario.obter(2));
    }
    
    @Test
    public void atacaComEspadaPerdeVida() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Auraniel");
        elfoDaLuz.atacarComEspada(new Dwarf("Gimli"));
        
        assertEquals(79.0, elfoDaLuz.getVida(), 1e-9);
    }
    
    @Test
    public void atacaComEspadaGanhaVida() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Auraniel");
        elfoDaLuz.atacarComEspada(new Dwarf("Gimli"));
        elfoDaLuz.atacarComEspada(new Dwarf("Gimli"));
        
        assertEquals(89.0, elfoDaLuz.getVida(), 1e-9);
    }
}
