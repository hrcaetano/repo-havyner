package LoTR;

import java.util.*;

public class EstrategiaAtaqueIntercalado implements EstrategiaDeAtaque {

    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
        
        ListIterator<Elfo> iterator = elfos.listIterator();
       
        //objetivo � comparar atual e pr�ximo. caso sejam iguais, move o atual para a posi��o do pr�ximo.
        //c�digo atual � um rascunho e carece de um algoritmo ordenador que funcione
        //causa erro, imagino que seja porque na primeira itera��o ainda n�o h� um objeto 'anterior'
        for( int i = 0; i < elfos.size(); i++) {
            if(iterator.previous().getClass() == iterator.next().getClass() ) {
                Collections.swap(elfos, i, i + 1 > elfos.size() ? i : i + 1); //tern�rio para evitar indexOutOfBounds
            }
        }
       
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes) {
        return ordenacao(atacantes);
    }
}
