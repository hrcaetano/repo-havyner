package LoTR;

 

public class Dwarf extends Personagem {
    
    public Dwarf( String nome ) {
        super(nome);
        this.vida = 110.0;
        this.quantidadeDano = 10.0;
        this.ganharItem(new Item(1, "Escudo de metal"));
    }
    
    public void equiparEscudo() {
        this.quantidadeDano = 5.0;
    }
    
    public String imprimirResumo() {
         return "Hey";
    }
}
