package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest {
   
    @Test
    public void dwarfNasceCom110DeVida() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        assertEquals(110.0, novoAnao.getVida(), 1e-9);
    }

    @Test
    public void dwarfPerde10DeVida() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        novoAnao.sofrerDano();
                
        assertEquals(100.0, novoAnao.getVida(), 1e-9);  
    }    
    
    @Test
    public void dwarfPerdeTodaVida11Ataques() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        for( int i = 0; i < 11; i++ ) {
            novoAnao.sofrerDano();
        }
                
        assertEquals(0.0, novoAnao.getVida(), 1e-9);  
    }
    
    @Test
    public void dwarfPerdeTodaVida12Ataques(){
        Dwarf dwarf = new Dwarf("Gimli");
        
        for( int i = 0; i < 12; i++ ) {
            dwarf.sofrerDano();
        }
        
        assertEquals(0.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNasceComStatus() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        assertEquals(Status.RECEM_CRIADO, novoAnao.getStatus());
    }

    @Test
    public void dwarfPerdeVidaEContinuaVivo() {
        Dwarf novoAnao = new Dwarf("Gimli");
        novoAnao.sofrerDano();
        
        assertEquals(Status.SOFREU_DANO, novoAnao.getStatus());
    }    
    
    @Test
    public void confereSeDwarfMorre() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        for( int i = 0; i < 11; i++ ) {
            novoAnao.sofrerDano();
        }
        
        assertEquals(Status.MORTO, novoAnao.getStatus());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf novoAnao = new Dwarf("Gimli");
        assertEquals("Escudo de metal", novoAnao.getInventario().busca("Escudo de metal").getDescricao());
    }
    
    @Test
    public void dwarfNasceuENaoEquipouEscudo() {
        Dwarf novoAnao = new Dwarf("Gimli");
        
        //assertFalse(novoAnao.isEquipado());
    }
    
    @Test
    public void equiparEscudo() {
        Dwarf novoAnao = new Dwarf("Gimli");
        novoAnao.equiparEscudo();
        
        //assertTrue(novoAnao.isEquipado());
    }
    
    @Test
    public void dwarfEquipadoSofreMenosDano() {
        Dwarf novoAnao = new Dwarf("Gimli");
        novoAnao.equiparEscudo();
        novoAnao.sofrerDano();
        
        assertEquals(105.0, novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf novoAnao = new Dwarf("Gimli");
        novoAnao.sofrerDano();
        
        assertEquals(100.0, novoAnao.getVida(), 1e-9);
    }
}
