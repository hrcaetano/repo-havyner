package LoTR;

 

public class ElfoNoturno extends Elfo {

    public ElfoNoturno( String nome ) {
        super(nome);
        this.quantidadeXpPorAtaque = 3;
        this.quantidadeDano = 15.0;
    }
}
