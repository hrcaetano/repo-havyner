package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest {

    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario(1);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        
        assertTrue(Double.isNaN(estatisticasInventario.calcularMedia()));        
    }
    
    @Test
    public void calcularMediaApensUmItem() {
        Inventario inventario = new Inventario(1);
        inventario.adicionar(new Item(2, "placeholder"));
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        
        assertEquals(2, estatisticasInventario.calcularMedia(), 1e-9);
    }
    
    @Test 
    public void calcularMediaDeDoisItens() {
        Item espada = new Item(1, "Espada longa");
        Item flechas = new Item(5, "Flechas para arco longo");
        Inventario inventario = new Inventario(1);
        inventario.adicionar(espada);
        inventario.adicionar(flechas);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        
        assertEquals(3, estatisticasInventario.calcularMedia(), 1e-9);
    }
    
    @Test
    public void tresQuantidadesAcimaDaMedia(){
        Item espada = new Item(1, "Espada longa");
        Item arco = new Item(4, "Arco longo");
        Item flechas = new Item(8, "Flechas para arco longo");
        Item machadinha = new Item(4, "Machadinha de arremesso");
        Item adaga = new Item(7, "Adaga");
        Inventario inventario = new Inventario(1);
        inventario.adicionar(espada);
        inventario.adicionar(arco);
        inventario.adicionar(flechas);
        inventario.adicionar(machadinha);
        inventario.adicionar(adaga);
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(inventario);
        
        assertEquals(2, estatisticasInventario.quantidadeItensAcimaDaMedia());
    }
}
