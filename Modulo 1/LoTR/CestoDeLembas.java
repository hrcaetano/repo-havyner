
public class CestoDeLembas {

    private int quantidade;
    
    public CestoDeLembas( int quantidade ) {
        this.quantidade = quantidade;
    }

    public boolean podeDividirEmPares() {
        
        if( this.quantidade > 2 && this.quantidade % 2 == 0 ) {
            return true;        
        } else {
            return false;        
        }
    }
}
