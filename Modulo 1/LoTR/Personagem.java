package LoTR;

 

public abstract class Personagem {

    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected double quantidadeDano;
    protected int experiencia;
    protected int quantidadeXpPorAtaque;
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        quantidadeDano = 0;
        quantidadeXpPorAtaque = 1;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
    public void sofrerDano() {
        if( this.podeSofrerDano() && this.quantidadeDano > 0.0 ) {
           this.vida = this.vida >= this.quantidadeDano ? this.vida - this.quantidadeDano : 0.0;
        }

        if( this.vida == 0.0 ) {
            this.status = Status.MORTO;
        } else {
            this.status = Status.SOFREU_DANO;
        }        
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar(item);
    }
    
    public void perderItem( Item item ) {  
        this.inventario.remover(item);
    }
    
    public void aumentarXp() {
         experiencia += quantidadeXpPorAtaque;
    }
    
    public abstract String imprimirResumo();
}
