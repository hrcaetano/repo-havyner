package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {

    @After
    public void tearDown() {
        System.gc();
    }

    @Test
    public void atiraFlechaDiminuirFlechaAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoAnao);
        
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQuantidadeFlecha());
    }


    @Test
    public void atirar2FlechasZerarFlechas2xAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoAnao);
        novoElfo.atirarFlecha(novoAnao);        
        
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQuantidadeFlecha());
    }
    
    @Test
    public void atirar3FlechasZerarFlechas2xAumentarXP() {
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoAnao);
        novoElfo.atirarFlecha(novoAnao); 
        novoElfo.atirarFlecha(novoAnao); 
        
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQuantidadeFlecha());
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado() {
        Elfo novoElfo = new Elfo("Legolas");
        
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void naoCriaElfoNaoIncrementa() {
        assertEquals(0, Elfo.getElfosInstanciados());        
    }
    
    @Test
    public void criaUmElfoContadorUmaVez() {
        Elfo novoElfo = new Elfo("Legolas");
        
        assertEquals(1, Elfo.getElfosInstanciados());   
    }
    
    @Test
    public void criaDoisElfosContadorUmaVez() {
        Elfo novoElfo = new Elfo("Legolas");
        Elfo novoElfo2 = new Elfo("Damriel");
        
        assertEquals(2, Elfo.getElfosInstanciados());  
    }
}