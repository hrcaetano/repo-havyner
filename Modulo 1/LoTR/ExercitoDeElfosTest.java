package LoTR;

 

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoDeElfosTest {
    
    @Test
    public void alistaElfoDoTipoCerto() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        exercitoDeElfos.alistar(elfoVerde);
        exercitoDeElfos.alistar(elfoNoturno);
        
        assertEquals(elfoVerde, exercitoDeElfos.getExercito().get(0));
        assertEquals(elfoNoturno, exercitoDeElfos.getExercito().get(1));
    }

    @Test
    public void alistaElfoDoTipoErrado() {
        ElfoDaLuz elfoDaLuz = new ElfoDaLuz("Auraniel");
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        exercitoDeElfos.alistar(elfoDaLuz);
        
        assertEquals(0, exercitoDeElfos.getExercito().size());
    }
    
    @Test
    public void buscarElfosPorStatusExistente() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        exercitoDeElfos.alistar(elfoVerde);        
        exercitoDeElfos.alistar(elfoNoturno);
        
        assertEquals(2, exercitoDeElfos.buscarPorStatus(Status.RECEM_CRIADO).size());
    }
    
    @Test
    public void buscarElfosPorStatusInexistente() {
        ElfoVerde elfoVerde = new ElfoVerde("Everon");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Drizzt");
        ExercitoDeElfos exercitoDeElfos = new ExercitoDeElfos();
        exercitoDeElfos.alistar(elfoVerde);        
        exercitoDeElfos.alistar(elfoNoturno);
        
        assertEquals(0, exercitoDeElfos.buscarPorStatus(Status.MORTO).size());    
    }
}
