package LoTR;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DataTerceiraEraTest {

    @Test
    public void isFimDaGuerraDoAnelBissexto() {
        DataTerceiraEra fimDaGuerraDoAnel = new DataTerceiraEra(1, 10, 3019);
        
        System.out.println(fimDaGuerraDoAnel.isBissexto());
        
        assertFalse(fimDaGuerraDoAnel.isBissexto());
    }
    
    @Test
    public void is2020Bissexto() {
        DataTerceiraEra anoQueVem = new DataTerceiraEra(12, 3, 2020);
                System.out.println(anoQueVem.isBissexto());
        assertTrue(anoQueVem.isBissexto());
    }
}
