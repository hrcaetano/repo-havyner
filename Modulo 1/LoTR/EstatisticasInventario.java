package LoTR;

 

import java.util.*;

public class EstatisticasInventario {
    
    private Inventario inventario;
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public EstatisticasInventario( Inventario inventario ) {
        this.inventario = inventario;
    }
    
    public double calcularMedia() {
        if( this.inventario.getItens().isEmpty() ) {
            return Double.NaN; 
        }
        
        int quantidadeObjetos = inventario.getItens().size();
        int totalItens = 0;
        
        for( int i = 0; i < quantidadeObjetos; i++ ) {
            totalItens += inventario.getItens().get(i).getQuantidade();
        }
        
        return (double) totalItens / (double) quantidadeObjetos;
    }
    
    public double calcularMediana() {
        if( this.inventario.getItens().isEmpty() ) {
            return Double.NaN; 
        }
        
        int quantidadeItens = this.inventario.getItens().size();
        int meio = quantidadeItens / 2;
        int quantidadeMeio = this.inventario.obter(meio).getQuantidade();
        
        if( quantidadeItens % 2 == 1 ) {
            return quantidadeMeio;
        }
       
        int quantidadeMeioMenosUm = this.inventario.obter( meio - 1 ).getQuantidade();
        
        return ( quantidadeMeio + quantidadeMeioMenosUm ) / 2.0;
    }
    
    public int quantidadeItensAcimaDaMedia() {
        double media = this.calcularMedia();
        int quantidadeAcima = 0;
        
        for( Item itemAtual : this.inventario.getItens() ) {
            if( itemAtual.getQuantidade() > media ) {
                quantidadeAcima++;
            }
        }
       
        return quantidadeAcima;
    }
}
