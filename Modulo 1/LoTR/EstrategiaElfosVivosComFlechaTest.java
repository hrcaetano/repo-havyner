package LoTR;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstrategiaElfosVivosComFlechaTest {
    
        @Test
        public void ordenarComTodosAptos() {
            EstrategiaElfosVivosComFlecha estrategia = new EstrategiaElfosVivosComFlecha();
            Elfo night1 = new ElfoNoturno("Noturno 1");
            night1.getInventario().busca("Flecha para arco longo").setQuantidade(10);
            Elfo night2 = new ElfoNoturno("Noturno 2");
            night2.getInventario().busca("Flecha para arco longo").setQuantidade(20);
            Elfo night3 = new ElfoNoturno("Noturno 3");
            night3.getInventario().busca("Flecha para arco longo").setQuantidade(30);
            Elfo night4 = new ElfoNoturno("Noturno 4");
            night4.getInventario().busca("Flecha para arco longo").setQuantidade(40);            
            Elfo green1 = new ElfoVerde("Verde 1");
            green1.getInventario().busca("Flecha para arco longo").setQuantidade(25);
            Elfo green2 = new ElfoVerde("Verde 2");
            green2.getInventario().busca("Flecha para arco longo").setQuantidade(15);            

            ArrayList<Elfo> elfosEnviados = new ArrayList<>(
                Arrays.asList(night1, night2, green1, night3, green2, night4)
            );
            ArrayList<Elfo> elfosEsperados = new ArrayList<>(
                Arrays.asList(green2, green1, night1)
            );
            ArrayList<Elfo> elfosResultado = estrategia.getOrdemAtaque(elfosEnviados);

            assertEquals(elfosEsperados, elfosResultado);
        }
        
}
