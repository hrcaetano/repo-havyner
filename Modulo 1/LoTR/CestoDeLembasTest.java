import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CestoDeLembasTest {

    @Test
    public void divideDoisLembas() {
        CestoDeLembas lembas = new CestoDeLembas(2);
        
        assertFalse(lembas.podeDividirEmPares());
    }

    @Test
    public void divideTresLembas() {
        CestoDeLembas lembas = new CestoDeLembas(3);
        
        assertFalse(lembas.podeDividirEmPares());    
    }

    @Test
    public void divideSeisLembas() {
        CestoDeLembas lembas = new CestoDeLembas(6);
        
        System.out.println(lembas.podeDividirEmPares());
        
        assertTrue(lembas.podeDividirEmPares());    
    }
}
