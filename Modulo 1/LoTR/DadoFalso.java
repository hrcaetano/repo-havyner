package LoTR;

public class DadoFalso implements Sorteador {
  
    private int valorFalso;
    
    public int sortear() {
        return this.valorFalso;
    }
    
    public void simularValor( int valor ) {
        this.valorFalso = valor;
    }
}
