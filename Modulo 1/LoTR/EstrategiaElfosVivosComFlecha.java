package LoTR;

import java.util.*;

public class EstrategiaElfosVivosComFlecha implements EstrategiaDeAtaque {

    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
        
        int quantidadeNoturnos = 0;
        int removerNoturnos = 0;
        
        Collections.sort(elfos);
        
        for( Elfo elfo : elfos ) {
            if( elfo.getStatus() == Status.MORTO && elfo.getFlecha().getQuantidade() <= 0 ) {
                elfos.remove(elfo);
            }
            
            if( elfo instanceof ElfoNoturno ) {
                quantidadeNoturnos++;
            }
        }
            
        if( quantidadeNoturnos > Math.floor(elfos.size()/3) ){
            
            removerNoturnos = quantidadeNoturnos - (int) Math.floor(elfos.size()/3);
            
            for( int i = 0; i < elfos.size() && removerNoturnos > 0; i++ ) {
                if( elfos.get(i) instanceof ElfoNoturno ){
                    elfos.remove(elfos.get(i));
                    removerNoturnos--;
                }
            }
        }
        
        return elfos;
    }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){
        return ordenacao(atacantes);
    }
}
