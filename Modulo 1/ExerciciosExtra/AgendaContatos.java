package ExerciciosExtra;

import java.util.*;
import java.util.Map;

public class AgendaContatos {

    private HashMap<String, String> contatos;
    
    public AgendaContatos() {
        contatos = new LinkedHashMap<>();
    }
    
    public void adicionarContato( String nome, String telefone ) {
        contatos.put(nome, telefone);
    }
    
    public String consultar( String nome ) {
        return contatos.get(nome);
    }
    
    public String consultarPorTelefone( String telefone ) {
        for (HashMap.Entry<String, String> par : contatos.entrySet()) {
            if (par.getValue().equals(telefone)) {
                return par.getValue();
            }
        }
        
        return null;
    }
    
    public String geraCSV() {
        StringBuilder csv = new StringBuilder();
        String separador = System.lineSeparator();
        
        for( HashMap.Entry<String, String> par : contatos.entrySet() ) {
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format( "%s,%s%s", chave, valor, separador );
            csv.append(contato);
        }
        
        return csv.toString();
    }
}
