package ExerciciosExtra;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
    
public class NumerosTest {
    
    @Test
    public void mediaUmNumero() {
        double[] entrada = new double[] { 1.5 };
        Numeros numeros = new Numeros(entrada);
        
        assertEquals(1.5, numeros.calcularMediaSeguinte()[0], 1e-9);
    }
    
    @Test
    public void mediaDoisNumeros() {
        double[] entrada = new double[] { 1.5, 2.0 };
        Numeros numeros = new Numeros(entrada);
        
        assertEquals(1.75, numeros.calcularMediaSeguinte()[0], 1e-9);
    }
    
    @Test
    public void mediaDoNumeroComOSeguinteQuatroPosicoes() {
        double[] entrada = new double[] { 1.5, 6.5, -5.0, 2.0 };
        Numeros numeros = new Numeros(entrada);
        
        assertEquals(4.0, numeros.calcularMediaSeguinte()[0], 1e-9);
        assertEquals(0.75, numeros.calcularMediaSeguinte()[1], 1e-9);
        assertEquals(-1.5, numeros.calcularMediaSeguinte()[2], 1e-9);
    }
    
    @Test
    public void mediaNumerosDadosNoEnunciado() {
        double[] entrada = new double[] { 1.0, 3.0, 5.0, 1.0, -10.0 };
        Numeros numeros = new Numeros(entrada);
        
        assertEquals(2.0, numeros.calcularMediaSeguinte()[0], 1e-9);
        assertEquals(4.0, numeros.calcularMediaSeguinte()[1], 1e-9);
        assertEquals(3.0, numeros.calcularMediaSeguinte()[2], 1e-9);
        assertEquals(-4.5, numeros.calcularMediaSeguinte()[3], 1e-9);
    }
}
