package ExerciciosExtra;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NumeroTest {

    @Test
    public void verificarSomaDivisivelPorNove() {
        Numero nove = new Numero(9);
        
        assertTrue(nove.verificarSomaDivisivel(1892376));
    }    
    
    @Test
    public void verificarSomaDivisivelPorTres() {
        Numero tres = new Numero(3);
        
        assertFalse(tres.verificarSomaDivisivel(17));    
    }
}
