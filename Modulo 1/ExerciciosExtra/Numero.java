package ExerciciosExtra;

import java.util.*;

public class Numero {

    private int numeroInicial;
    
    public Numero( int numeroInicial ) {
        this.numeroInicial = numeroInicial;
    }
    
    public int getNumeroInicial() {
        return this.numeroInicial;
    }
    
    public boolean verificarSomaDivisivel( int numeroParaComparar ) {
        
        int length = (int) ( Math.log10(numeroParaComparar) + 1 );
        int soma = 0;
        int numeroIterado = numeroParaComparar;
        
        for( int i = 0; i < length; i++ ){
            //System.out.println(numeroParaComparar % 10);
            //System.out.println(numeroParaComparar);
            
            soma += numeroIterado % 10;
            numeroIterado = numeroIterado / 10;
        }
        
        System.out.println(soma);
        
        if( soma % this.getNumeroInicial() == 0 ) {
            return true;       
        } else {
            return false;        
        }
    }
}
