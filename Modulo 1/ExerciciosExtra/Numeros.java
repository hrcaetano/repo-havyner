package ExerciciosExtra;

public class Numeros {

    private double[] arrayDeNumeros;
    
    public Numeros( double[] arrayDeNumeros ) {
        this.arrayDeNumeros = arrayDeNumeros;
    }
    
    public double[] calcularMediaSeguinte() {
        double[] mediaNumeroComSeguinte = new double[ this.arrayDeNumeros.length - 1 ];
        int indexArrayResposta = 0;       
        
        if( arrayDeNumeros.length == 1 ) {
            mediaNumeroComSeguinte = new double[1];
            mediaNumeroComSeguinte[0] = arrayDeNumeros[0];
        } else if( arrayDeNumeros.length == 2 ) {
            mediaNumeroComSeguinte[0] = (arrayDeNumeros[0] + arrayDeNumeros[1]) / 2;
        } else {
            for( int i = 0; i < this.arrayDeNumeros.length - 1; i++ ) {
               mediaNumeroComSeguinte[indexArrayResposta] = 
                    (arrayDeNumeros[i] + arrayDeNumeros[i + 1]) / 2;
               indexArrayResposta++;
               
               if( indexArrayResposta == arrayDeNumeros.length - 1 ) {
                   break;
                }
            }
        }
        
        return mediaNumeroComSeguinte;
    }
}
