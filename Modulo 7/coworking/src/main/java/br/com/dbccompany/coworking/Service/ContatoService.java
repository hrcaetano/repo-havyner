package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvar( Contato contato ) {
        return contatoRepository.save(contato);
    }

    @Transactional( rollbackFor = Exception.class )
    public Contato editar( Contato contato, Integer id ) {
        contato.setId(id);

        return contatoRepository.save(contato);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        contatoRepository.deleteById(id);
    }

    public Contato findByValor( String valor ) {
        return contatoRepository.findByValor(valor);
    }

    public List<Contato> todosContatos() {
        return (List<Contato>) contatoRepository.findAll();
    }

    public Contato contatoEspecifico( Integer id ) {
        Optional<Contato> contato = contatoRepository.findById(id);

        return contato.get();
    }
}