package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {

    Clientes findByNome(String nome);
    Clientes findByCpf(String cpf);
    Clientes findByDataDeNascimento(String dataDeNascimento);
    List<Clientes> findAll();
}
