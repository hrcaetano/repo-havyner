package br.com.dbccompany.coworking.Entity;

public enum TipoContracao {
    MINUTOS, HORAS, TURNOS, DIARIAS, SEMANAS, MESES
}
