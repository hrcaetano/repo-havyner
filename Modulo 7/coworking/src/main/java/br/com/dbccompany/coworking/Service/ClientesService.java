package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Transactional( rollbackFor = Exception.class )
    public Clientes salvar( Clientes clientes ) {
        return clientesRepository.save(clientes);
    }

    @Transactional( rollbackFor = Exception.class )
    public Clientes editar( Clientes clientes, Integer id ) {
        clientes.setId(id);

        boolean flagEmail = false;
        boolean flagTelefone = false;
        List<Contato> contatos = clientes.getContato();

        if( contatos.size() >= 2 ) {
            for( Contato contato : contatos ) {
                if( contato.getTipoContato().getNome().equals("email") ) {
                    flagEmail = true;
                }

                if( contato.getTipoContato().getNome().equals("telefone") ) {
                    flagTelefone = true;
                }
            }

            if( !(flagEmail && flagTelefone )) {
                throw new RuntimeException("Ao ser criado o Cliente necessita ter contatos tipo 'email' e 'telefone'.");
            }
        }

        return clientesRepository.save(clientes);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        clientesRepository.deleteById(id);
    }

    public Clientes findByNome( String nome ) {
        return clientesRepository.findByNome(nome);
    }

    public Clientes findByCpf( String cpf ) {
        return clientesRepository.findByCpf(cpf);
    }

    public Clientes findByDataDeNascimento( String data ) {
        return clientesRepository.findByDataDeNascimento(data);
    }

    public List<Clientes> todosClientes() {
        return (List<Clientes>) clientesRepository.findAll();
    }

    public Clientes clienteEspecifico( Integer id ) {
        Optional<Clientes> clientes = clientesRepository.findById(id);

        return clientes.get();
    }
}