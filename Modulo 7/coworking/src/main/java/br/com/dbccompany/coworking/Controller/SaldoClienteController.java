package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Service.PagamentosService;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/saldocliente" )
public class SaldoClienteController {

    @Autowired
    SaldoClienteService saldoClienteService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<SaldoCliente> todosSaldosClientes() {
        return saldoClienteService.todosSaldosClientes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public SaldoCliente saldoClienteEspecifico(@PathVariable SaldoClienteKey key) {
        return saldoClienteService.saldosClientesEspecifico(key);
    }

    @GetMapping( value = "/tipoContracao/{tipoContracao}" )
    @ResponseBody
    public SaldoCliente saldoClientePorTipoContracao(@PathVariable TipoContracao tipoContracao) {
        return saldoClienteService.findByTipoContracao( tipoContracao );
    }

    @GetMapping( value = "/vencimento/{vencimento}" )
    @ResponseBody
    public SaldoCliente saldoClientePorVencimento(@PathVariable String vencimento) {
        return saldoClienteService.findByVencimento( vencimento );
    }

    @GetMapping( value = "/quantidade/{quantidade}" )
    @ResponseBody
    public SaldoCliente saldoClientePorQuantidade(@PathVariable Integer quantidade) {
        return saldoClienteService.findByQuantidade( quantidade );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.salvar(saldoCliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable SaldoClienteKey key, @RequestBody SaldoCliente saldoCliente) {
        return saldoClienteService.editar(saldoCliente, key);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerSaldoCliente(@PathVariable SaldoClienteKey key) {
        saldoClienteService.remover(key);

        return true;
    }
}