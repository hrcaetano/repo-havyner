package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "Clientes")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Clientes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Clientes_Seq", sequenceName = "Clientes_Seq" )
    @GeneratedValue( generator = "Clientes_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true )
    private String cpf;

    @Column( name = "data_nascimento", nullable = false )
    private Date dataDeNascimento;

    @OneToMany(mappedBy = "clientes")
    Set<ClientesPacotes> clientesPacotes;

    @OneToMany(mappedBy = "clientes")
    Set<SaldoCliente> saldoCliente;

    @OneToMany(mappedBy = "clientes")
    List<Contato> contato;

    @OneToMany(mappedBy = "clientes")
    Set<Contratacao> contratacoes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Date dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public Set<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(Set<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Set<SaldoCliente> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Set<SaldoCliente> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<Contato> getContato() {
        return contato;
    }

    public void setContato(List<Contato> contato) {
        this.contato = contato;
    }

    public Set<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(Set<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }
}
