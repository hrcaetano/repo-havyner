package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Contratacao salvar( Contratacao contratacao ) {
        return contratacaoRepository.save(contratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public Contratacao editar(Contratacao contratacao, Integer id ) {
        contratacao.setId(id);

        return contratacaoRepository.save(contratacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        contratacaoRepository.deleteById(id);
    }

    public Contratacao findByQuantidade(Integer quantidade) {
        return contratacaoRepository.findByQuantidade(quantidade);
    }

    public Contratacao findByDesconto(Double desconto) {
        return contratacaoRepository.findByDesconto(desconto);
    }

    public Contratacao findByPrazo(Integer prazo) {
        return contratacaoRepository.findByPrazo(prazo);
    }

    public List<Contratacao> todasContratacoes() {
        return (List<Contratacao>) contratacaoRepository.findAll();
    }

    public Contratacao contratacaoEspecifica( Integer id ) {
        Optional<Contratacao> contratacao = contratacaoRepository.findById(id);

        return contratacao.get();
    }

    public Double valorDocontrato(Integer espacosId, Integer contratacaoId) {
        Optional<Espacos> espacos = espacosRepository.findById(espacosId);
        Optional<Contratacao> contratacao = contratacaoRepository.findById(contratacaoId);
        Double resultado = espacos.get().getValor() * contratacao.get().getQuantidade();
        Double desconto;

        if( contratacao.get().getDesconto() > 0 ) {
            desconto = contratacao.get().getDesconto();
            resultado = resultado - ( resultado * desconto );
        }

        return resultado;
    }
}