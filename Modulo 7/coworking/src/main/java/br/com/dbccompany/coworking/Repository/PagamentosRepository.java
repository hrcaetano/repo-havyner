package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {

    Pagamentos findByTipoPagamento(TipoPagamento tipoPagamento);
    List<Pagamentos> findAll();
}
