package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "Espacos")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Espacos {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Espacos_Seq", sequenceName = "Espacos_Seq" )
    @GeneratedValue( generator = "Espacos_Seq", strategy = GenerationType.SEQUENCE )
    Integer id;

    @Column( nullable = false, unique = true )
    String nome;

    @Column( name = "qtd_pessoas", nullable = false)
    Double quantidadePessoas;

    @Column( nullable = false )
    Double valor;

    @OneToMany(mappedBy = "espacos")
    Set<EspacosPacotes> espacosPacotes;

    @OneToMany(mappedBy = "espacos")
    Set<SaldoCliente> saldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantidadePessoas() {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Double quantidadePessoas) {
        this.quantidadePessoas = quantidadePessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(Set<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public Set<SaldoCliente> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Set<SaldoCliente> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
