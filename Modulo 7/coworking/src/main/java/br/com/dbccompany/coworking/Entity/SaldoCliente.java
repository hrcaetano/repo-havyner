package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "Saldo_Cliente")
public class SaldoCliente {

    @EmbeddedId
    @SequenceGenerator( allocationSize = 1, name = "Saldo_Cliente_Seq", sequenceName = "Saldo_Cliente_Seq" )
    @GeneratedValue( generator = "Saldo_Cliente_Seq", strategy = GenerationType.SEQUENCE )
    private SaldoClienteKey id;

    @Enumerated(EnumType.STRING)
    @Column( name = "tipo_contratacao", nullable = false )
    private TipoContracao tipoContracao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private Date vencimento;

    @ManyToOne
    @MapsId("id_cliente")
    @JoinColumn(name = "id_cliente")
    Clientes clientes;

    @ManyToOne
    @MapsId("id_espaco")
    @JoinColumn(name = "id_espaco")
    Espacos espacos;

    public SaldoClienteKey getId() {
        return id;
    }

    public void setId(SaldoClienteKey id) {
        this.id = id;
    }

    public TipoContracao getTipoContracao() {
        return tipoContracao;
    }

    public void setTipoContracao(TipoContracao tipoContracao) {
        this.tipoContracao = tipoContracao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
