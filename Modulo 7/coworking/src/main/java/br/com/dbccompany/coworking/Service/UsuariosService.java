package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import br.com.dbccompany.coworking.Util.Criptografia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {

    @Autowired
    private UsuariosRepository usuariosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Usuarios salvar(Usuarios usuarios ) {
        String senhaDesprotegida = usuarios.getSenha();

        if( senhaDesprotegida.length() >= 6 && StringUtils.isAlphanumeric(senhaDesprotegida) ) {
            usuarios.setSenha(Criptografia.criptografar(senhaDesprotegida));
        } else {
            throw new RuntimeException("A senha deve consistir de no mínimo seis caracteres alfanuméricos.");
        }

        return usuariosRepository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class )
    public Usuarios editar( Usuarios usuarios, Integer id ) {
        usuarios.setId(id);

        return usuariosRepository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        usuariosRepository.deleteById(id);
    }

    public Usuarios findByNome( String nome ) {
        return usuariosRepository.findByNome(nome);
    }

    public Usuarios findByEmail( String email ) {
        return usuariosRepository.findByEmail(email);
    }

    public Usuarios findByLogin( String login ) {
        return usuariosRepository.findByLogin(login);
    }

    public List<Usuarios> todosUsuarios() {
        return (List<Usuarios>) usuariosRepository.findAll();
    }

    public Usuarios clienteEspecifico( Integer id ) {
        Optional<Usuarios> usuarios = usuariosRepository.findById(id);

        return usuarios.get();
    }
}