package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteKey;
import br.com.dbccompany.coworking.Entity.TipoContracao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, SaldoClienteKey> {

    SaldoCliente findByTipoContracao(TipoContracao tipoContracao);
    SaldoCliente findByQuantidade(Integer quantidade);
    SaldoCliente findByVencimento(String vencimento);
    List<SaldoCliente> findAll();
}
