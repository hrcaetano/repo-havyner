    package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table( name = "Contratacao" )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Contratacao {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Contratacao_Seq", sequenceName = "Contratacao_Seq" )
    @GeneratedValue( generator = "Contratacao_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContracao tipoContracao;

    private Integer quantidade;

    private Double desconto;

    private Integer prazo;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_clientes")
    private Clientes clientes;

    @ManyToOne( cascade = CascadeType.MERGE)
    @JoinColumn( name = "id_espacos")
    private Espacos espacos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContracao getTipoContracao() {
        return tipoContracao;
    }

    public void setTipoContracao(TipoContracao tipoContracao) {
        this.tipoContracao = tipoContracao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }
}
