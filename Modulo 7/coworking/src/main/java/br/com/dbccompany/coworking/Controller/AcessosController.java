package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessosController {

    @Autowired
    AcessosService acessosService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Acessos> todosAcessoss() {
        return acessosService.todosAcessos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Acessos acessosEspecifico(@PathVariable Integer id) {
        return acessosService.acessosEspecifico(id);
    }

    @GetMapping( value = "/data/{data}" )
    @ResponseBody
    public Acessos acessosPelaData(@PathVariable String data) {
        return acessosService.findByData( data );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Acessos novoAcessos(@RequestBody Acessos acessos) {
        return acessosService.salvar(acessos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Acessos editarAcessos(@PathVariable Integer id, @RequestBody Acessos acessos) {
        return acessosService.editar(acessos, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerAcessos(@PathVariable Integer id) {
        acessosService.remover(id);

        return true;
    }
}