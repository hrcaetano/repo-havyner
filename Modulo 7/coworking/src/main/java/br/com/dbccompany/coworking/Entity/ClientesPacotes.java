package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table( name = "Clientes_Pacotes")
@JsonIdentityInfo(scope = ClientesPacotes.class, generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class ClientesPacotes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Clientes_Pacotes_Seq", sequenceName = "Clientes_Pacotes_Seq" )
    @GeneratedValue( generator = "Clientes_Pacotes_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private Integer quantidade;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    Clientes clientes;

    @ManyToOne
    @JoinColumn(name = "id_pacote")
    Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}
