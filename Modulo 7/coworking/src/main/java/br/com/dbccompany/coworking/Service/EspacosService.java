package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Espacos salvar( Espacos espacos ) {
        return espacosRepository.save(espacos);
    }

    @Transactional( rollbackFor = Exception.class )
    public Espacos editar( Espacos espacos, Integer id ) {
        espacos.setId(id);

        return espacosRepository.save(espacos);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        espacosRepository.deleteById(id);
    }

    public Espacos findByValor( Double valor ) {
        return espacosRepository.findByValor(valor);
    }

    public Espacos findByQuantidadePessoas(Integer quantidadePessoas) {
        return espacosRepository.findByQuantidadePessoas(quantidadePessoas);
    }

    public Espacos findByNome(String nome) {
        return espacosRepository.findByNome(nome);
    }

    public List<Espacos> todosEspacos() {
        return (List<Espacos>) espacosRepository.findAll();
    }

    public Espacos espacoEspecifico( Integer id ) {
        Optional<Espacos> espacos = espacosRepository.findById(id);

        return espacos.get();
    }
}