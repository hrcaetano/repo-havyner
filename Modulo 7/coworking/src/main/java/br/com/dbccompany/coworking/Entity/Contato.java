package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table( name = "Contato")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Contato {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Contato_Seq", sequenceName = "Contato_Seq" )
    @GeneratedValue( generator = "Contato_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private String valor;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "Fk_Id_Tipo_Contato", nullable = false )
    private TipoContato tipoContato;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "Fk_Id_Cliente", nullable = false )
    private Clientes clientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public Clientes getClientes() {
        return clientes;
    }

    public void setClientes(Clientes clientes) {
        this.clientes = clientes;
    }
}
