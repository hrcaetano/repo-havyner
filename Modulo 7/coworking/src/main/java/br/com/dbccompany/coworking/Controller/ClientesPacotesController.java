package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientespacotes" )
public class ClientesPacotesController {

    @Autowired
    ClientesPacotesService clientesPacotesService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ClientesPacotes> todosClientesPacotes() {
        return clientesPacotesService.todosClientesPacotes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public ClientesPacotes clientesPacotesEspecifico(@PathVariable Integer id) {
        return clientesPacotesService.clientesPacotesEspecifico(id);
    }

    @GetMapping( value = "/quantidade/{quantidade}" )
    @ResponseBody
    public ClientesPacotes clientesPacotesPorQuantidade(@PathVariable Integer quantidade) {
        return clientesPacotesService.findByQuantidade( quantidade );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesPacotes novoClientesPacotes(@RequestBody ClientesPacotes clientesPacotes) {
        return clientesPacotesService.salvar(clientesPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public ClientesPacotes editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotes clientesPacotes) {
        return clientesPacotesService.editar(clientesPacotes, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerClientesPacotes(@PathVariable Integer id) {
        clientesPacotesService.remover(id);

        return true;
    }
}