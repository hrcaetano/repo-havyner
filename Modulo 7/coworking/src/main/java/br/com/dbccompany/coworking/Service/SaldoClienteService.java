package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteKey;
import br.com.dbccompany.coworking.Entity.TipoContracao;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {

    @Autowired
    private SaldoClienteRepository saldoClienteRepository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente salvar(SaldoCliente saldoCliente ) {
        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente editar( SaldoCliente saldoCliente, SaldoClienteKey key ) {
        saldoCliente.setId(key);

        return saldoClienteRepository.save(saldoCliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( SaldoClienteKey key ) {
        saldoClienteRepository.deleteById(key);
    }

    public SaldoCliente findByTipoContracao( TipoContracao tipoContracao ) {
        return saldoClienteRepository.findByTipoContracao(tipoContracao);
    }

    public SaldoCliente findByQuantidade(Integer quantidade) {
        return saldoClienteRepository.findByQuantidade(quantidade);
    }

    public SaldoCliente findByVencimento(String vencimento) {
        return saldoClienteRepository.findByVencimento(vencimento);
    }

    public List<SaldoCliente> todosSaldosClientes() {
        return (List<SaldoCliente>) saldoClienteRepository.findAll();
    }

    public SaldoCliente saldosClientesEspecifico( SaldoClienteKey key ) {
        Optional<SaldoCliente> saldoCliente = saldoClienteRepository.findById(key);

        return saldoCliente.get();
    }
}