package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class )
    public TipoContato salvar(TipoContato tipoContato ) {
        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoContato editar( TipoContato tipoContato, Integer id ) {
        tipoContato.setId(id);

        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        tipoContatoRepository.deleteById(id);
    }

    public TipoContato findByNome( String nome ) {
        return tipoContatoRepository.findByNome(nome);
    }

    public List<TipoContato> todosTipoContatos() {
        return (List<TipoContato>) tipoContatoRepository.findAll();
    }

    public TipoContato tipoContatoEspecifico( Integer id ) {
        Optional<TipoContato> tipoContato = tipoContatoRepository.findById(id);

        return tipoContato.get();
    }
}