package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Service.ClientesService;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/usuarios" )
public class UsuariosController {

    @Autowired
    UsuariosService usuariosService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Usuarios> todosUsuarios() {
        return usuariosService.todosUsuarios();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Usuarios usuariosEspecifico(@PathVariable Integer id) {
        return usuariosService.clienteEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Usuarios usuariosPeloNome(@PathVariable String nome) {
        return usuariosService.findByNome( nome );
    }

    @GetMapping( value = "/email/{email}" )
    @ResponseBody
    public Usuarios usuariosPeloEmail(@PathVariable String email) {
        return usuariosService.findByEmail( email );
    }

    @GetMapping( value = "/login/{login}" )
    @ResponseBody
    public Usuarios usuariosPeloLogin(@PathVariable String login) {
        return usuariosService.findByLogin( login );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Usuarios novoUsuarios(@RequestBody Usuarios usuarios) {
        return usuariosService.salvar(usuarios);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Usuarios editarUsuarios(@PathVariable Integer id, @RequestBody Usuarios usuarios) {
        return usuariosService.editar(usuarios, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerUsuarios(@PathVariable Integer id) {
        usuariosService.remover(id);

        return true;
    }
}