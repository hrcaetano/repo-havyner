package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {

    Pacotes findByValor(Double valor);
    List<Pacotes> findAll();
}
