package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Entity.TipoContracao;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacospacotes" )
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService espacosPacotesService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<EspacosPacotes> todosEspacosPacotes() {
        return espacosPacotesService.todosEspacosPacotes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public EspacosPacotes espacosPacotesEspecifico(@PathVariable Integer id) {
        return espacosPacotesService.espacosPacotesEspecifico(id);
    }

    @GetMapping( value = "/quantidade/{quantidade}" )
    @ResponseBody
    public EspacosPacotes espacosPacotesPorQuantidade(@PathVariable Integer quantidade) {
        return espacosPacotesService.findByQuantidade( quantidade );
    }

    @GetMapping( value = "/tipoContracao/{tipoContracao}" )
    @ResponseBody
    public EspacosPacotes espacosPacotesPorTipoContracao(@PathVariable TipoContracao tipoContracao) {
        return espacosPacotesService.findByTipoContracao( tipoContracao );
    }

    @GetMapping( value = "/prazo/{prazo}" )
    @ResponseBody
    public EspacosPacotes espacosPacotesPorTipoContracao(@PathVariable Integer prazo) {
        return espacosPacotesService.findByPrazo( prazo );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosPacotes novoEspacosPacotes(@RequestBody EspacosPacotes espacosPacotes) {
        return espacosPacotesService.salvar(espacosPacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public EspacosPacotes editarEspacosPacotes(@PathVariable Integer id, @RequestBody EspacosPacotes espacosPacotes) {
        return espacosPacotesService.editar(espacosPacotes, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerEspacosPacotes(@PathVariable Integer id) {
        espacosPacotesService.remover(id);

        return true;
    }
}