package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table( name = "Tipo_Contato")
public class TipoContato {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Tipo_Contato_Seq", sequenceName = "Tipo_Contato_Seq" )
    @GeneratedValue( generator = "Tipo_Contato_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private String nome;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
