package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pagamentos" )
public class PagamentosController {

    @Autowired
    PagamentosService pagamentosService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Pagamentos> todosPagamentos() {
        return pagamentosService.todosPagamentos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pagamentos pagamentoEspecifico(@PathVariable Integer id) {
        return pagamentosService.pacoteEspecifico(id);
    }

    @GetMapping( value = "/tipoPagamento/{tipoPagamento}" )
    @ResponseBody
    public Pagamentos pagamentosPorValor(@PathVariable TipoPagamento tipoPagamento) {
        return pagamentosService.findByTipoPagamento( tipoPagamento );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Pagamentos novoPagamentos(@RequestBody Pagamentos pagamentos) {
        return pagamentosService.salvar(pagamentos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pagamentos editarPagamentos(@PathVariable Integer id, @RequestBody Pagamentos pagamentos) {
        return pagamentosService.editar(pagamentos, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerPagamentos(@PathVariable Integer id) {
        pagamentosService.remover(id);

        return true;
    }
}