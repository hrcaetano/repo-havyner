package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pacotes" )
public class PacotesController {

    @Autowired
    PacotesService pacotesService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Pacotes> todosPacotes() {
        return pacotesService.todosPacotes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pacotes espacoEspecifico(@PathVariable Integer id) {
        return pacotesService.pacoteEspecifico(id);
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public Pacotes pacotesPorValor(@PathVariable Double valor) {
        return pacotesService.findByValor( valor );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Pacotes novoPacotes(@RequestBody Pacotes pacotes) {
        return pacotesService.salvar(pacotes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pacotes editarPacotes(@PathVariable Integer id, @RequestBody Pacotes pacotes) {
        return pacotesService.editar(pacotes, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerPacotes(@PathVariable Integer id) {
        pacotesService.remover(id);

        return true;
    }
}