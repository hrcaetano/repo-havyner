package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    private AcessosRepository acessosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Acessos salvar( Acessos acessos ) {
        if( acessos.getData() == null ) {
            acessos.setData(new Date());
        }

        return acessosRepository.save(acessos);
    }

    @Transactional( rollbackFor = Exception.class )
    public Acessos editar( Acessos acessos, Integer id ) {
        acessos.setId(id);

        return acessosRepository.save(acessos);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        acessosRepository.deleteById(id);
    }

    public Acessos findByData( String data ) {
        return acessosRepository.findByData(data);
    }

    public List<Acessos> todosAcessos() {
        return (List<Acessos>) acessosRepository.findAll();
    }

    public Acessos acessosEspecifico( Integer id ) {
        Optional<Acessos> acessos = acessosRepository.findById(id);

        return acessos.get();
    }
}