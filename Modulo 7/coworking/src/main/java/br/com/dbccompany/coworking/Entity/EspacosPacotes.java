package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

@Entity
@Table( name = "Espacos_Pacotes")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class EspacosPacotes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Espacos_Pacotes_Seq", sequenceName = "Espacos_Pacotes_Seq" )
    @GeneratedValue( generator = "Espacos_Pacotes_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column( name = "tipo_contratacao")
    private TipoContracao tipoContracao;

    private Integer quantidade;

    private Integer prazo;

    @ManyToOne
    @JoinColumn(name = "id_espaco")
    Espacos espacos;

    @ManyToOne
    @JoinColumn(name = "id_pacote")
    Pacotes pacotes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContracao getTipoContracao() {
        return tipoContracao;
    }

    public void setTipoContracao(TipoContracao tipoContracao) {
        this.tipoContracao = tipoContracao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Pacotes getPacotes() {
        return pacotes;
    }

    public void setPacotes(Pacotes pacotes) {
        this.pacotes = pacotes;
    }
}
