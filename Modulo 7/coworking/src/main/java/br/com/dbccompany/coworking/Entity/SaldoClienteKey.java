package br.com.dbccompany.coworking.Entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SaldoClienteKey implements Serializable {

    @Column(name = "id_cliente")
    Long clienteId;

    @Column(name = "id_espaco")
    Long espacoId;

    public Long getClienteId() {
        return clienteId;
    }

    public void setClienteId(Long clienteId) {
        this.clienteId = clienteId;
    }

    public Long getEspacoId() {
        return espacoId;
    }

    public void setEspacoId(Long espacoId) {
        this.espacoId = espacoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SaldoClienteKey)) return false;
        SaldoClienteKey that = (SaldoClienteKey) o;
        return getClienteId().equals(that.getClienteId()) &&
                getEspacoId().equals(that.getEspacoId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getClienteId(), getEspacoId());
    }
}
