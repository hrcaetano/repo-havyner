package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {

    Usuarios findByNome(String nome);
    Usuarios findByEmail(String email);
    Usuarios findByLogin(String login);
    List<Usuarios> findAll();
}
