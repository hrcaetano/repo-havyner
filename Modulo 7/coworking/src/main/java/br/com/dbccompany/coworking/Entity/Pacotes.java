package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "Pacotes")
public class Pacotes {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Pacotes_Seq", sequenceName = "Pacotes_Seq" )
    @GeneratedValue( generator = "Pacotes_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
