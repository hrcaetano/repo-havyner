package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Entity.TipoContracao;
import br.com.dbccompany.coworking.Repository.EspacosPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosPacotesService {

    @Autowired
    private EspacosPacotesRepository espacosPacotesRepository;

    @Transactional( rollbackFor = Exception.class )
    public EspacosPacotes salvar( EspacosPacotes espacosPacotes ) {
        return espacosPacotesRepository.save(espacosPacotes);
    }

    @Transactional( rollbackFor = Exception.class )
    public EspacosPacotes editar(EspacosPacotes espacosPacotes, Integer id ) {
        espacosPacotes.setId(id);

        return espacosPacotesRepository.save(espacosPacotes);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        espacosPacotesRepository.deleteById(id);
    }

    public EspacosPacotes findByQuantidade(Integer quantidade) {
        return espacosPacotesRepository.findByQuantidade(quantidade);
    }

    public EspacosPacotes findByTipoContracao(TipoContracao tipoContracao) {
        return espacosPacotesRepository.findByTipoContracao(tipoContracao);
    }

    public EspacosPacotes findByPrazo(Integer prazo) {
        return espacosPacotesRepository.findByPrazo(prazo);
    }

    public List<EspacosPacotes> todosEspacosPacotes() {
        return (List<EspacosPacotes>) espacosPacotesRepository.findAll();
    }

    public EspacosPacotes espacosPacotesEspecifico( Integer id ) {
        Optional<EspacosPacotes> espacosPacotes = espacosPacotesRepository.findById(id);

        return espacosPacotes.get();
    }
}