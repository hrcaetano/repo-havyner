package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contratacao" )
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class ContratacaoController {

    @Autowired
    ContratacaoService contratacaoService;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Contratacao> todasContratacoes() {
        return contratacaoService.todasContratacoes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Contratacao contratacaoEspecifica(@PathVariable Integer id) {
        return contratacaoService.contratacaoEspecifica(id);
    }

    @GetMapping( value = "/quantidade/{quantidade}" )
    @ResponseBody
    public Contratacao findByQuantidade(@PathVariable Integer quantidade) {
        return contratacaoService.findByQuantidade( quantidade );
    }

    @GetMapping( value = "/prazo/{prazo}" )
    @ResponseBody
    public Contratacao contratacaoPorPrazo(@PathVariable Integer prazo) {
        return contratacaoService.findByPrazo( prazo );
    }

    @GetMapping( value = "/desconto/{desconto}" )
    @ResponseBody
    public Contratacao contratacaoPorDesconto(@PathVariable Double desconto) {
        return contratacaoService.findByDesconto( desconto );
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public Contratacao novaContratacao(@RequestBody Contratacao contratacao) {
        return contratacaoService.salvar(contratacao);
    }

    @PostMapping( value = "/orcamento")
    @ResponseBody
    public Double novoOrcamento(@RequestBody Integer espacoId, @RequestBody Integer contratacaoId) {
        return contratacaoService.valorDocontrato(espacoId, contratacaoId);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
        return contratacaoService.editar(contratacao, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerContratacao(@PathVariable Integer id) {
        contratacaoService.remover(id);

        return true;
    }
}