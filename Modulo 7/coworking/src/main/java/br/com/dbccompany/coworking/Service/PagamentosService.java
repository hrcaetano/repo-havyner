package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Entity.TipoPagamento;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {

    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Transactional( rollbackFor = Exception.class )
    public Pagamentos salvar(Pagamentos pagamentos ) {
        return pagamentosRepository.save(pagamentos);
    }

    @Transactional( rollbackFor = Exception.class )
    public Pagamentos editar( Pagamentos pagamentos, Integer id ) {
        pagamentos.setId(id);

        return pagamentosRepository.save(pagamentos);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        pagamentosRepository.deleteById(id);
    }

    public Pagamentos findByTipoPagamento( TipoPagamento tipoPagamento ) {
        return pagamentosRepository.findByTipoPagamento(tipoPagamento);
    }

    public List<Pagamentos> todosPagamentos() {
        return (List<Pagamentos>) pagamentosRepository.findAll();
    }

    public Pagamentos pacoteEspecifico( Integer id ) {
        Optional<Pagamentos> pagamentos = pagamentosRepository.findById(id);

        return pagamentos.get();
    }
}