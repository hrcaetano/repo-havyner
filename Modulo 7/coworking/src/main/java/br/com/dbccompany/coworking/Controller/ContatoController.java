package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/contato" )
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Contato> todosContatos() {
        return contatoService.todosContatos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Contato contatoEspecifico(@PathVariable Integer id) {
        return contatoService.contatoEspecifico(id);
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public Contato contatoPorValor(@PathVariable String valor) {
        return contatoService.findByValor( valor );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Contato novoContato(@RequestBody Contato contato) {
        return contatoService.salvar(contato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato contato) {
        return contatoService.editar(contato, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerContato(@PathVariable Integer id) {
        contatoService.remover(id);

        return true;
    }
}