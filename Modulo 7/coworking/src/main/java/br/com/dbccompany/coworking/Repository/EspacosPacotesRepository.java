package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Entity.TipoContracao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotes, Integer> {

    EspacosPacotes findByTipoContracao(TipoContracao tipoContracao);
    EspacosPacotes findByQuantidade(Integer quantidade);
    EspacosPacotes findByPrazo(Integer prazo);
    List<EspacosPacotes> findAll();
}
