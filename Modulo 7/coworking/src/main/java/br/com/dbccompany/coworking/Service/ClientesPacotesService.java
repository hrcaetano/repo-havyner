package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesPacotesService {

    @Autowired
    private ClientesPacotesRepository clientesPacotesRepository;

    @Transactional( rollbackFor = Exception.class )
    public ClientesPacotes salvar( ClientesPacotes clientesPacotes ) {
        return clientesPacotesRepository.save(clientesPacotes);
    }

    @Transactional( rollbackFor = Exception.class )
    public ClientesPacotes editar( ClientesPacotes clientesPacotes, Integer id ) {
        clientesPacotes.setId(id);

        return clientesPacotesRepository.save(clientesPacotes);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover( Integer id ) {
        clientesPacotesRepository.deleteById(id);
    }

    public ClientesPacotes findByQuantidade( Integer quantidade ) {
        return clientesPacotesRepository.findByQuantidade(quantidade);
    }

    public List<ClientesPacotes> todosClientesPacotes() {
        return (List<ClientesPacotes>) clientesPacotesRepository.findAll();
    }

    public ClientesPacotes clientesPacotesEspecifico( Integer id ) {
        Optional<ClientesPacotes> clientesPacotes = clientesPacotesRepository.findById(id);

        return clientesPacotes.get();
    }
}