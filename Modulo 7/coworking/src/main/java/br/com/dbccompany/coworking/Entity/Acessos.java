package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table( name = "Acessos")
public class Acessos {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "Acessos_Seq", sequenceName = "Acessos_Seq" )
    @GeneratedValue(generator = "Acessos_Seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( name = "is_entrada")
    private Boolean isEntrada;

    private Date data;

    @Column( name = "is_excecao")
    private Boolean isExcecao;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_cliente")
    private Clientes idClienteSaldoCliente;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "id_espaco")
    private Espacos idEspacosSaldoCliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

    public Clientes getIdClienteSaldoCliente() {
        return idClienteSaldoCliente;
    }

    public void setIdClienteSaldoCliente(Clientes idClienteSaldoCliente) {
        this.idClienteSaldoCliente = idClienteSaldoCliente;
    }

    public Espacos getIdEspacosSaldoCliente() {
        return idEspacosSaldoCliente;
    }

    public void setIdEspacosSaldoCliente(Espacos idEspacosSaldoCliente) {
        this.idEspacosSaldoCliente = idEspacosSaldoCliente;
    }
}
