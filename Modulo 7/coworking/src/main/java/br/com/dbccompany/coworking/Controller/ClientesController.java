package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/clientes" )
public class ClientesController {

    @Autowired
    ClientesService clientesService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Clientes> todosClientess() {
        return clientesService.todosClientes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Clientes clientesEspecifico(@PathVariable Integer id) {
        return clientesService.clienteEspecifico(id);
    }

    @GetMapping( value = "/data/{data}" )
    @ResponseBody
    public Clientes clientesPelaDataNascimento(@PathVariable String data) {
        return clientesService.findByDataDeNascimento( data );
    }

    @GetMapping( value = "/cpf/{cpf}" )
    @ResponseBody
    public Clientes clientesPeloCpf(@PathVariable String cpf) {
        return clientesService.findByCpf( cpf );
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Clientes clientesPeloNome(@PathVariable String nome) {
        return clientesService.findByNome( nome );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Clientes novoClientes(@RequestBody Clientes clientes) {
        return clientesService.salvar(clientes);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Clientes editarClientes(@PathVariable Integer id, @RequestBody Clientes clientes) {
        return clientesService.editar(clientes, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerClientes(@PathVariable Integer id) {
        clientesService.remover(id);

        return true;
    }
}