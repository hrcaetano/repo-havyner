package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.PacotesService;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipocontato" )
public class TipoContatoController {

    @Autowired
    TipoContatoService tipoContatoService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoContato> todosTipoContatos() {
        return tipoContatoService.todosTipoContatos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public TipoContato tipoContatoEspecifico(@PathVariable Integer id) {
        return tipoContatoService.tipoContatoEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public TipoContato tipoContatoPorNome(@PathVariable String nome) {
        return tipoContatoService.findByNome( nome );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public TipoContato novoTipoContato(@RequestBody TipoContato tipoContato) {
        return tipoContatoService.salvar(tipoContato);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoContato editarTipoContato(@PathVariable Integer id, @RequestBody TipoContato tipoContato) {
        return tipoContatoService.editar(tipoContato, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerTipoContato(@PathVariable Integer id) {
        tipoContatoService.remover(id);

        return true;
    }
}