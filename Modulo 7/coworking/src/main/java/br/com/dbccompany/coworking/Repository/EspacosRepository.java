package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {

    Espacos findByValor(Double valor);
    Espacos findByQuantidadePessoas(Integer quantidadePessoas);
    Espacos findByNome(String nome);
    List<Espacos> findAll();
}
