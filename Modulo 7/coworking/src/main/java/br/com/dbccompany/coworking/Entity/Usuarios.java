package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "Usuarios")
public class Usuarios {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "Usuarios_Seq", sequenceName = "Usuarios_Seq" )
    @GeneratedValue( generator = "Usuarios_Seq", strategy = GenerationType.SEQUENCE )
    private Integer id;

    @Column( nullable = false )
    private String nome;

    @Column( nullable = false, unique = true )
    private String email;

    @Column( nullable = false, unique = true )
    private String login;

    @Column( nullable = false )
    private String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
