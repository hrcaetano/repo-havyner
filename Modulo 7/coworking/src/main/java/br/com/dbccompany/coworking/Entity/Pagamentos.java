package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "Pagamentos")
public class Pagamentos {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "Pagamento_Seq", sequenceName = "Pagamento_Seq" )
    @GeneratedValue(generator = "Pagamento_Seq", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_pagamento")
    private TipoPagamento tipoPagamento;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "id_clientes_pacotes")
    private ClientesPacotes clientesPacotes;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "id_contratacao")
    private Contratacao contratacao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }
}
