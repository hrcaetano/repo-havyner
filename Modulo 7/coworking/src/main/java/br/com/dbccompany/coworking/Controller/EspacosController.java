package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/espacos" )
public class EspacosController {

    @Autowired
    EspacosService espacosService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Espacos> todosEspacos() {
        return espacosService.todosEspacos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Espacos espacoEspecifico(@PathVariable Integer id) {
        return espacosService.espacoEspecifico(id);
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public Espacos espacosPorValor(@PathVariable Double valor) {
        return espacosService.findByValor( valor );
    }

    @GetMapping( value = "/quantidadePessoas/{quantidadePessoas}" )
    @ResponseBody
    public Espacos espacosPorValor(@PathVariable Integer quantidadePessoas) {
        return espacosService.findByQuantidadePessoas( quantidadePessoas );
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Espacos espacosPorNome(@PathVariable String nome) {
        return espacosService.findByNome( nome );
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Espacos novoEspacos(@RequestBody Espacos espacos) {
        return espacosService.salvar(espacos);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Espacos editarEspacos(@PathVariable Integer id, @RequestBody Espacos espacos) {
        return espacosService.editar(espacos, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerEspacos(@PathVariable Integer id) {
        espacosService.remover(id);

        return true;
    }
}