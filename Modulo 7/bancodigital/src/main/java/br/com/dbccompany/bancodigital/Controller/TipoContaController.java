package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.TipoConta;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.TipoContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/tipoconta" )
public class TipoContaController {

    @Autowired
    TipoContaService tipoContaService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<TipoConta> todosTipoContas() {
        return tipoContaService.todosTipoContas();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public TipoConta tipoContaEspecifico(@PathVariable Integer id) {
        return tipoContaService.tipoContaEspecifico(id);
    }

    @GetMapping( value = "/descricao/{descricao}" )
    @ResponseBody
    public TipoConta tipoContaPeloNome(@PathVariable String descricao) {
        return tipoContaService.findByDescricao(descricao);
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public TipoConta novoTipoConta(@RequestBody TipoConta tipoConta) {
        return tipoContaService.salvar(tipoConta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public TipoConta editarTipoConta(@PathVariable Integer id, @RequestBody TipoConta tipoConta) {
        return tipoContaService.editar(tipoConta, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerTipoConta(@PathVariable Integer id) {
        tipoContaService.remover(id);

        return true;
    }
}
