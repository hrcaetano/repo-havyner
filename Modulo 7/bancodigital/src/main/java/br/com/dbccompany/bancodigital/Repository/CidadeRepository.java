package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cidade;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadeRepository extends CrudRepository<Cidade, Integer> {

    Cidade findByNome(String nome);
    List<Cidade> findAll();
}
