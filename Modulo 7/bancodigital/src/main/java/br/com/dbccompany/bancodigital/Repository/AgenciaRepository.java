package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Banco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciaRepository extends CrudRepository<Agencia, Integer> {

    Agencia findByNome(String nome);
    List<Agencia> findAll();
}
