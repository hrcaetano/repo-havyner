package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository bancoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Banco salvar( Banco banco ) {
        return bancoRepository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public Banco editar( Banco banco, Integer id ) {
        banco.setId(id);

        return bancoRepository.save(banco);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        bancoRepository.deleteById(id);
    }

    public List<Banco> todosBancos() {
        return (List<Banco>) bancoRepository.findAll();
    }

    public Banco bancoEspecifico( Integer id ) {
        Optional<Banco> banco = bancoRepository.findById(id);

        return banco.get();
    }

    public Banco findByNome(String nome) {
        return bancoRepository.findByNome(nome);
    }
}
