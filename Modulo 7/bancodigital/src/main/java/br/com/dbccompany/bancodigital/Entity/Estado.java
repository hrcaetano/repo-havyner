package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "ESTADO" )
public class Estado {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "ESTADO_SEQ", sequenceName = "ESTADO_SEQ")
    @GeneratedValue( generator = "ESTADO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_ESTADO", nullable = false)
    Integer id;

    String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_PAIS" )
    private Pais pais;

    @OneToMany( mappedBy = "estado")
    private List<Cidade> cidades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
}
