package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.TipoConta;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.TipoContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContaService {

    @Autowired
    private TipoContaRepository tipoContaRepository;

    @Transactional( rollbackFor = Exception.class )
    public TipoConta salvar( TipoConta tipoConta ) {
        return tipoContaRepository.save(tipoConta);
    }

    @Transactional( rollbackFor = Exception.class )
    public TipoConta editar( TipoConta tipoConta, Integer id ) {
        tipoConta.setId(id);

        return tipoContaRepository.save(tipoConta);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        tipoContaRepository.deleteById(id);
    }

    public List<TipoConta> todosTipoContas() {
        return (List<TipoConta>) tipoContaRepository.findAll();
    }

    public TipoConta tipoContaEspecifico(Integer id ) {
        Optional<TipoConta> tipoConta = tipoContaRepository.findById(id);

        return tipoConta.get();
    }

    public TipoConta findByDescricao(String descricao) {
        return tipoContaRepository.findByDescricao(descricao);
    }
}
