package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EstadoService {

    @Autowired
    private EstadoRepository estadoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Estado salvar(Estado estado ) {
        return estadoRepository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public Estado editar( Estado estado, Integer id ) {
        estado.setId(id);

        return estadoRepository.save(estado);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        estadoRepository.deleteById(id);
    }

    public List<Estado> todosEstados() {
        return (List<Estado>) estadoRepository.findAll();
    }

    public Estado estadoEspecifico( Integer id ) {
        Optional<Estado> estado = estadoRepository.findById(id);

        return estado.get();
    }

    public Estado findByNome(String nome) {
        return estadoRepository.findByNome(nome);
    }
}
