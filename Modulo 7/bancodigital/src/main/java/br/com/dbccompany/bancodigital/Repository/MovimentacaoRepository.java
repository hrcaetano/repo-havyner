package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Integer> {

    Movimentacao findByValor(Double valor);
    Movimentacao findByTipoMovimentacao(TipoMovimentacao tipoMovimentacao);
    List<Movimentacao> findAll();
}
