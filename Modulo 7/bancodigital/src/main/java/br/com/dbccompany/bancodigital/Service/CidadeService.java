package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.CidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CidadeService {

    @Autowired
    private CidadeRepository cidadeRepository;

    @Transactional( rollbackFor = Exception.class )
    public Cidade salvar( Cidade cidade ) {
        return cidadeRepository.save(cidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public Cidade editar( Cidade cidade, Integer id ) {
        cidade.setId(id);

        return cidadeRepository.save(cidade);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        cidadeRepository.deleteById(id);
    }

    public List<Cidade> todasCidades() {
        return (List<Cidade>) cidadeRepository.findAll();
    }

    public Cidade cidadeEspecifica(Integer id ) {
        Optional<Cidade> cidade = cidadeRepository.findById(id);

        return cidade.get();
    }

    public Cidade findByNome(String nome) {
        return cidadeRepository.findByNome(nome);
    }
}
