package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Entity.Pais;
import br.com.dbccompany.bancodigital.Service.EstadoService;
import br.com.dbccompany.bancodigital.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/pais" )
public class PaisController {

    @Autowired
    PaisService paisService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Pais> todosPaises() {
        return paisService.todosPaises();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Pais paisEspecifico(@PathVariable Integer id) {
        return paisService.paisEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Pais paisPeloNome(@PathVariable String nome) {
        return paisService.findByNome(nome);
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Pais novoPais(@RequestBody Pais pais) {
        return paisService.salvar(pais);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Pais editarPais(@PathVariable Integer id, @RequestBody Pais pais) {
        return paisService.editar(pais, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerPais(@PathVariable Integer id) {
        paisService.remover(id);

        return true;
    }
}
