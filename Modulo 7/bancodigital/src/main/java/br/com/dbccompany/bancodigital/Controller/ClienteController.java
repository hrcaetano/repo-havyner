package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cliente" )
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Cliente> todosClientes() {
        return clienteService.todosClientes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Cliente clienteEspecifico(@PathVariable Integer id) {
        return clienteService.clienteEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Cliente clientePeloNome(@PathVariable String nome) {
        return clienteService.findByNome(nome);
    }

    @GetMapping( value = "/cpf/{cpf}" )
    @ResponseBody
    public Cliente clientePeloCpf(@PathVariable String cpf) {
        return clienteService.findByCpf(cpf);
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente cliente) {
        return clienteService.salvar(cliente);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cliente editarCliente(@PathVariable Integer id, @RequestBody Cliente cliente) {
        return clienteService.editar(cliente, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerCliente(@PathVariable Integer id) {
        clienteService.remover(id);

        return true;
    }
}
