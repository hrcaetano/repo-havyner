package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco" )
public class BancoController {

    @Autowired
    BancoService bancoService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Banco> todosBancos() {
        return bancoService.todosBancos();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Banco bancoEspecifico(@PathVariable Integer id) {
        return bancoService.bancoEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Banco bancoPeloNome(@PathVariable String nome) {
        return bancoService.findByNome(nome);
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Banco novoBanco(@RequestBody Banco banco) {
        return bancoService.salvar(banco);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Banco editarBanco(@PathVariable Integer id, @RequestBody Banco banco) {
        return bancoService.editar(banco, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerBanco(@PathVariable Integer id) {
        bancoService.remover(id);

        return true;
    }
}
