package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Cliente;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Transactional( rollbackFor = Exception.class )
    public Cliente salvar( Cliente cliente ) {
        return clienteRepository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public Cliente editar(Cliente cliente, Integer id ) {
        cliente.setId(id);

        return clienteRepository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        clienteRepository.deleteById(id);
    }

    public List<Cliente> todosClientes() {
        return (List<Cliente>) clienteRepository.findAll();
    }

    public Cliente clienteEspecifico( Integer id ) {
        Optional<Cliente> cliente = clienteRepository.findById(id);

        return cliente.get();
    }

    public Cliente findByNome(String nome) {
        return clienteRepository.findByNome(nome);
    }

    public Cliente findByCpf(String cpf) {
        return clienteRepository.findByCpf(cpf);
    }
}
