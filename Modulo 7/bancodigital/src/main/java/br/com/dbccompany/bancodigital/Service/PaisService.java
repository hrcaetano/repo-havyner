package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Pais;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PaisService {

    @Autowired
    private PaisRepository paisRepository;

    @Transactional( rollbackFor = Exception.class )
    public Pais salvar(Pais pais ) {
        return paisRepository.save(pais);
    }

    @Transactional( rollbackFor = Exception.class )
    public Pais editar( Pais pais, Integer id ) {
        pais.setId(id);

        return paisRepository.save(pais);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        paisRepository.deleteById(id);
    }

    public List<Pais> todosPaises() {
        return (List<Pais>) paisRepository.findAll();
    }

    public Pais paisEspecifico( Integer id ) {
        Optional<Pais> pais = paisRepository.findById(id);

        return pais.get();
    }

    public Pais findByNome(String nome) {
        return paisRepository.findByNome(nome);
    }
}
