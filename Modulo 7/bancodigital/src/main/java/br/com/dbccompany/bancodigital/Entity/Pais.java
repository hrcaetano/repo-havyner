package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "PAIS" )
public class Pais {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "PAIS_SEQ", sequenceName = "PAIS_SEQ")
    @GeneratedValue( generator = "PAIS_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_PAIS", nullable = false)
    Integer id;

    String nome;

    @OneToMany( mappedBy = "pais")
    private List<Estado> estados;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }
}
