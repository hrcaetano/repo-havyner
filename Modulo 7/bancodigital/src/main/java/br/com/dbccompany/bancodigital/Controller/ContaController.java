package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/conta" )
public class ContaController {

    @Autowired
    ContaService contaService;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Conta> todasContas() {
        return contaService.todasContas();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Conta contaEspecifica(@PathVariable Integer id) {
        return contaService.contaEspecifica(id);
    }

    @GetMapping( value = "/numero/{numero}" )
    @ResponseBody
    public Conta contaPeloNumero(@PathVariable Integer numero) {
        return contaService.findByNumero(numero);
    }

    @GetMapping( value = "/saldo/{saldo}" )
    @ResponseBody
    public Conta contaPeloSaldo(@PathVariable Double saldo) {
        return contaService.findBySaldo(saldo);
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public Conta novaConta(@RequestBody Conta conta) {
        return contaService.salvar(conta);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Conta editarConta(@PathVariable Integer id, @RequestBody Conta conta) {
        return contaService.editar(conta, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerConta(@PathVariable Integer id) {
        contaService.remover(id);
        
        return true;
    }
}
