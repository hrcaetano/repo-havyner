package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "CIDADE" )
public class Cidade {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADE_SEQ", sequenceName = "CIDADE_SEQ")
    @GeneratedValue( generator = "CIDADE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_CIDADE", nullable = false)
    Integer id;

    String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESTADO" )
    private Estado estado;

    @OneToMany( mappedBy = "cidade")
    private List<Agencia> agencias;

    @ManyToMany
    @JoinTable(
            name = "CIDADE_X_CLIENTE",
            joinColumns = @JoinColumn(name = "CIDADE_ID"),
            inverseJoinColumns = @JoinColumn(name = "CLIENTE_ID")
    )
    private List<Cliente> clientes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Agencia> getAgencias() {
        return agencias;
    }

    public void setAgencias(List<Agencia> agencias) {
        this.agencias = agencias;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
