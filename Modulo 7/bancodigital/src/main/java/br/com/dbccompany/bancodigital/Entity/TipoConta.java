package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "TIPO_CONTA")
public class TipoConta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue( generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_TIPO_CONTA", nullable = false)
    Integer id;

    String descricao;

    @OneToMany( mappedBy = "tipoConta")
    private List<Conta> contas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
