package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Service.AgenciaService;
import br.com.dbccompany.bancodigital.Service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/agencia" )
public class AgenciaController {

    @Autowired
    AgenciaService agenciaService;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Agencia> todasAgencias() {
        return agenciaService.todasAgencias();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Agencia agenciaEspecifica(@PathVariable Integer id) {
        return agenciaService.agenciaEspecifica(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Agencia agenciaPeloNome(@PathVariable String nome) {
        return agenciaService.findByNome(nome);
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public Agencia novaAgencia(@RequestBody Agencia agencia) {
        return agenciaService.salvar(agencia);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Agencia editarAgencia(@PathVariable Integer id, @RequestBody Agencia agencia) {
        return agenciaService.editar(agencia, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerAgencia(@PathVariable Integer id) {
        agenciaService.remover(id);
        
        return true;
    }
}
