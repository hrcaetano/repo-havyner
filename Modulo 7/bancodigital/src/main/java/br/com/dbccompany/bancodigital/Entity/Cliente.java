package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "CLIENTE")
public class Cliente {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_CLIENTE", nullable = false)
    Integer id;

    String cpf;

    String nome;

    @ManyToMany
    @JoinTable(
            name = "CONTA_X_CLIENTE",
            joinColumns = @JoinColumn(name = "cliente_id"),
            inverseJoinColumns = @JoinColumn(name = "conta_id"))
    Set<Conta> contas;

    @ManyToMany( mappedBy = "clientes")
    private List<Cidade> cidades;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set<Conta> getContas() {
        return contas;
    }

    public void setContas(Set<Conta> contas) {
        this.contas = contas;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
}
