package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Entity.TipoMovimentacao;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacaoService {

    @Autowired
    private MovimentacaoRepository movimentacaoRepository;

    @Transactional( rollbackFor = Exception.class )
    public Movimentacao salvar( Movimentacao movimentacao ) {
        return movimentacaoRepository.save(movimentacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public Movimentacao editar( Movimentacao movimentacao, Integer id ) {
        movimentacao.setId(id);

        return movimentacaoRepository.save(movimentacao);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        movimentacaoRepository.deleteById(id);
    }

    public List<Movimentacao> todasMovimentacoes() {
        return (List<Movimentacao>) movimentacaoRepository.findAll();
    }

    public Movimentacao movimentacaoEspecifica(Integer id ) {
        Optional<Movimentacao> movimentacao = movimentacaoRepository.findById(id);

        return movimentacao.get();
    }

    public Movimentacao findByValor(Double valor) {
        return movimentacaoRepository.findByValor(valor);
    }

    public Movimentacao findByTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        return movimentacaoRepository.findByTipoMovimentacao(tipoMovimentacao);
    }
}
