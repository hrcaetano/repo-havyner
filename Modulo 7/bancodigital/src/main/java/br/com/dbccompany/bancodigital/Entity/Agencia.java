package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "AGENCIA" )
public class Agencia {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "AGENCIA_SEQ", sequenceName = "AGENCIA_SEQ")
    @GeneratedValue( generator = "AGENCIA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_AGENCIA", nullable = false)
    Integer id;

    String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_BANCO" )
    private Banco banco;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CIDADE" )
    private Cidade cidade;

    @OneToMany( mappedBy = "agencia")
    private List<Conta> contas = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }
}
