package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table( name = "BANCO" )
public class Banco {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue( generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_BANCO", nullable = false)
    Integer id;

    String nome;

    @OneToMany( mappedBy = "banco")
    private List<Agencia> agencia;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Agencia> getAgencia() {
        return agencia;
    }

    public void setAgencia(List<Agencia> agencia) {
        this.agencia = agencia;
    }
}
