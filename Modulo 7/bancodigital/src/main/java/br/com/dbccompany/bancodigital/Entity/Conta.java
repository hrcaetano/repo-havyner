package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table( name = "CONTA")
public class Conta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue( generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_CONTA", nullable = false)
    Integer id;

    Integer numero;

    Double saldo;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_TIPO_CONTA" )
    private TipoConta tipoConta;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_AGENCIA" )
    private Agencia agencia;

    @OneToMany( mappedBy = "conta")
    private List<Movimentacao> movimentacoes;

    @ManyToMany(mappedBy = "contas")
    List<Cliente> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public List<Movimentacao> getMovimentacoes() {
        return movimentacoes;
    }

    public void setMovimentacoes(List<Movimentacao> movimentacoes) {
        this.movimentacoes = movimentacoes;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
