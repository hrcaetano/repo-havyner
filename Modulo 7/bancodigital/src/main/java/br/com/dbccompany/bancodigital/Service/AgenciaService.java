package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Repository.AgenciaRepository;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AgenciaService {

    @Autowired
    private AgenciaRepository agenciaRepository;

    @Transactional( rollbackFor = Exception.class )
    public Agencia salvar( Agencia agencia ) {
        return agenciaRepository.save(agencia);
    }

    @Transactional( rollbackFor = Exception.class )
    public Agencia editar( Agencia agencia, Integer id ) {
        agencia.setId(id);

        return agenciaRepository.save(agencia);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        agenciaRepository.deleteById(id);
    }

    public List<Agencia> todasAgencias() {
        return (List<Agencia>) agenciaRepository.findAll();
    }

    public Agencia agenciaEspecifica( Integer id ) {
        Optional<Agencia> agencia = agenciaRepository.findById(id);

        return agencia.get();
    }

    public Agencia findByNome(String nome) {
        return agenciaRepository.findByNome(nome);
    }
}
