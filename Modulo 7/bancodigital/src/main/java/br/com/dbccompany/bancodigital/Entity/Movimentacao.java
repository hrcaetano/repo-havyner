package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;

@Entity
@Table( name = "MOVIMENTACAO")
public class Movimentacao {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
    @GeneratedValue( generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE )
    @Column(name = "ID_MOVIMENTACAO", nullable = false)
    Integer id;

    Double valor;

    TipoMovimentacao tipoMovimentacao;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CONTA" )
    private Conta conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
