package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Agencia;
import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Service.AgenciaService;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/cidade" )
public class CidadeController {

    @Autowired
    CidadeService cidadeService;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Cidade> todasCidades() {
        return cidadeService.todasCidades();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Cidade cidadeEspecifica(@PathVariable Integer id) {
        return cidadeService.cidadeEspecifica(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Cidade cidadePeloNome(@PathVariable String nome) {
        return cidadeService.findByNome(nome);
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public Cidade novaCidade(@RequestBody Cidade cidade) {
        return cidadeService.salvar(cidade);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Cidade editarCidade(@PathVariable Integer id, @RequestBody Cidade cidade) {
        return cidadeService.editar(cidade, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerCidade(@PathVariable Integer id) {
        cidadeService.remover(id);
        
        return true;
    }
}
