package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/estado" )
public class EstadoController {

    @Autowired
    EstadoService estadoService;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Estado> todosEstados() {
        return estadoService.todosEstados();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Estado estadoEspecifico(@PathVariable Integer id) {
        return estadoService.estadoEspecifico(id);
    }

    @GetMapping( value = "/nome/{nome}" )
    @ResponseBody
    public Estado estadoPeloNome(@PathVariable String nome) {
        return estadoService.findByNome(nome);
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public Estado novoEstado(@RequestBody Estado estado) {
        return estadoService.salvar(estado);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Estado editarEstado(@PathVariable Integer id, @RequestBody Estado estado) {
        return estadoService.editar(estado, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerEstado(@PathVariable Integer id) {
        estadoService.remover(id);

        return true;
    }
}
