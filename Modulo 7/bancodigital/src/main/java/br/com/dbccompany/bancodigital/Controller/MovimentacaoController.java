package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Cidade;
import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Entity.TipoMovimentacao;
import br.com.dbccompany.bancodigital.Service.CidadeService;
import br.com.dbccompany.bancodigital.Service.MovimentacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/movimentacao" )
public class MovimentacaoController {

    @Autowired
    MovimentacaoService movimentacaoService;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Movimentacao> todasMovimentacaos() {
        return movimentacaoService.todasMovimentacoes();
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Movimentacao movimentacaoEspecifica(@PathVariable Integer id) {
        return movimentacaoService.movimentacaoEspecifica(id);
    }

    @GetMapping( value = "/valor/{valor}" )
    @ResponseBody
    public Movimentacao movimentacaoPeloNome(@PathVariable Double valor) {
        return movimentacaoService.findByValor(valor);
    }

    @GetMapping( value = "/tipo/{tipo}" )
    @ResponseBody
    public Movimentacao movimentacaoPeloTipo(@PathVariable TipoMovimentacao tipo) {
        return movimentacaoService.findByTipoMovimentacao(tipo);
    }

    @PostMapping( value = "/nova")
    @ResponseBody
    public Movimentacao novaMovimentacao(@RequestBody Movimentacao movimentacao) {
        return movimentacaoService.salvar(movimentacao);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public Movimentacao editarMovimentacao(@PathVariable Integer id, @RequestBody Movimentacao movimentacao) {
        return movimentacaoService.editar(movimentacao, id);
    }

    @DeleteMapping( value = "/remover/{id}" )
    @ResponseBody
    public Boolean removerMovimentacao(@PathVariable Integer id) {
        movimentacaoService.remover(id);
        
        return true;
    }
}
