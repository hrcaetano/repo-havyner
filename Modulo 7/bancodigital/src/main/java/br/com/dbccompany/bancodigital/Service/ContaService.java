package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Banco;
import br.com.dbccompany.bancodigital.Entity.Conta;
import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.Repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    private ContaRepository contaRepository;

    @Transactional( rollbackFor = Exception.class )
    public Conta salvar(Conta conta ) {
        return contaRepository.save(conta);
    }

    @Transactional( rollbackFor = Exception.class )
    public Conta editar( Conta conta, Integer id ) {
        conta.setId(id);

        return contaRepository.save(conta);
    }

    @Transactional( rollbackFor = Exception.class )
    public void remover(Integer id ) {
        contaRepository.deleteById(id);
    }

    public List<Conta> todasContas() {
        return (List<Conta>) contaRepository.findAll();
    }

    public Conta contaEspecifica( Integer id ) {
        Optional<Conta> conta = contaRepository.findById(id);

        return conta.get();
    }

    public Conta findByNumero(Integer numero) {
        return contaRepository.findByNumero(numero);
    }

    public Conta findBySaldo(Double saldo) {
        return contaRepository.findBySaldo(saldo);
    }
}
